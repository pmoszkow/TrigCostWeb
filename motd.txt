M6 data will be found <a href="https://atlas-trig-cost.cern.ch/?dir=M6">here</a>.
M7 data will be found <a href="https://atlas-trig-cost.cern.ch/?dir=M7">here</a>, and M6 data <a href="https://atlas-trig-cost.cern.ch/?dir=M6">here</a>.
Links to <a href="https://atlas-trig-cost.cern.ch/?dir=M8-M9">M8-M9</a>, <a href="https://atlas-trig-cost.cern.ch/?dir=M7">M7</a> and <a href="https://atlas-trig-cost.cern.ch/?dir=M6">M6</a> data.
Automated processing of data from P1 is in place, working through recent <a href="https://atlas-trig-cost.cern.ch/?dir=data15-cos">data15-cos</a> backlog.
Recent P1 data may be found at <a href="https://atlas-trig-cost.cern.ch/?dir=data15-13TeV">data15-13TeV</a>.
<a href="https://atlas-trig-cost.cern.ch/?dir=data15-5TeV">5 TeV pp-ref</a> and <a href="https://atlas-trig-cost.cern.ch/?dir=data15-hi">Heavy Ion</a> data has been added.
You can now <a href="?page=CompareRates">Compare Rates</a> between processing on the site.
There is now a <a href="https://atlas-trig-cost.cern.ch/?dir=TrigP1Test-RTT">RTT test for CPU consumption</a> (unprescaled).
