# This script runs the analysis

################################################## ##################################################
HOME=/opt/trig-cost-sw
################################################## ##################################################

echo "Execute] Manual invoked cost processing"

# Go home
cd $HOME

#setup eos
echo "Execute] Setup EOS"
#export PATH=$PATH:/afs/cern.ch/project/eos/installation/pro/bin/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/project/eos/installation/pro/lib64/
export EOS_MGM_URL=root://eosatlas.cern.ch

kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab # Authenticate

# Prepare run command
echo "Execute] SCRIPT: $1"
source $1


# Check straight away for another thing to process
echo "breakSleep" > /var/www/TrigCostWeb/breakSleep

