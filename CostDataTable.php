<?php
class CostDataTable {

  var $tableTitle;
  var $tableName;

  var $csvFile;
  var $dir;
  var $type;
  var $tag;
  var $run;
  var $range;
  var $level;
  var $summary;
  var $item;

  var $csvFileB;
  var $dirB;
  var $typeB;
  var $tagB;
  var $runB;
  var $rangeB;
  var $levelB;
  var $summaryB;
  var $itemB;
  
  var $searchable;
  var $toHide;
  
  var $sortDir;
  var $sortCol;
  
  var $doComp; // Do comparison between two rates
  var $compHeaderSetup;
  var $compLumi; // Rates comparison target lumi
  var $lumiA; // Actual lumi of comparison A
  var $lumiB; // Actual lumi of comparison B

  function __construct() {
    $this->searchable[0] = TRUE;
    $this->sortDir = "asc";
    $this->sortCol = 0;
    $this->doComp = false;
    $this->compLumi = 0;
    $this->lumiA = 0;
    $this->lumiB = 0;
    $this->compHeaderSetup = "Name,Group,Matched,Same ID,Rate A [Hz],Rate Err A [Hz],Rate B [Hz],Rate Err B [Hz],Prescale A, Prescale B,Difference (A-B) [Hz],Abs. Difference Sig. [|σ|],Ratio (A/B),Ratio Err";
  }
  
  function addSearchCol($canBeSearced) {
    $this->searchable[intval($canBeSearced)] = TRUE;
  }

  function hideColumn($toHide) {
    $this->toHide[intval($toHide)] = TRUE;
  }
  
  function setSortCol($col, $order) {
    assert($order == 'asc' || $order == 'desc');
    $this->sortDir = $order;
    $this->sortCol = intval($col);
  }

  
  function setTitle($title) {
    $this->tableTitle = $title;
    $this->tableName = str_replace(' ', '', $title);
  }
  
  function setDataSource( $csvFile, $dir = "", $type = "", $tag = "", $run = "", $range = "", $level = "", $summary = "", $item = "" ) {
    $this->dir = $dir;
    $this->type = $type;
    $this->tag = $tag;
    $this->run = $run;
    $this->range = $range;
    $this->level = $level;
    $this->summary = $summary;
    $this->item = $item;
    $this->csvFile = $csvFile;
  }

  function setComparison( $csvFile, $dir = "", $type = "", $tag = "", $run = "", $range = "", $level = "", $summary = "", $item = "" ) {
    $this->dirB = $dir;
    $this->typeB = $type;
    $this->tagB = $tag;
    $this->runB = $run;
    $this->rangeB = $range;
    $this->levelB = $level;
    $this->summaryB = $summary;
    $this->itemB = $item;
    $this->csvFileB = $csvFile;
    $this->doComp = true;
  }

  function setCompLumi($lumi, $lumiA, $lumiB) {
    $this->compLumi = $lumi;
    $this->lumiA = $lumiA;
    $this->lumiB = $lumiB;
  }
  
  function getHeaderArray() {
    if ($this->doComp == true) return str_getcsv($this->compHeaderSetup);    
    $csv = fopen($this->csvFile, 'r');
    $line = fgets($csv);
    fclose($csv);
    return str_getcsv($line);
  } 
  
  function getFooterArray() {
    if ($this->doComp == true) return str_getcsv($this->compHeaderSetup);    
    $csv = fopen($this->csvFile, 'r');
    $line = fgets($csv);
    $line = fgets($csv);
    fclose($csv);
    return str_getcsv($line);
  } 

  function insertRatesPiChart() {
    // We are going to call the table generation script inline!
    // This will get us the data we can parse to make a pi chart
    $_GET['rp'] = 500; // Results to return
    $_GET['query'] = false; //no search
    $_GET['qtype'] = -1; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $this->csvFile;
    $_GET['inline'] = TRUE;
    // Get the data
    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result = json_decode($jsonResult)->{"rows"}; // Convert to array - get data (only one entry)
    if (count($result) == 0) return false; // No data to display
    $groupRates = array();
    $L0Rate = 0.;
    $L1Rate = 0.;
    $HLTRate = 0.;
    $PhysicsRate = 0.;
    // Need to locate the correct column
    $arrayLocation = array_search("Weighted PS Rate [Hz]", $this->getHeaderArray());
    foreach($result as $row) {
      foreach($row as $key => $data) {
        if ($key == 'id') continue;
        $name = $data[0];
        $rate = $data[  $arrayLocation ];
        if ( strpos($name, "CPS") !== FALSE) continue; // Don't put CPS groups in the graphic
        if ( strpos($name, "STREAM_") !== FALSE) continue; // Don't put Streams in the graphic
        if (abs($rate) < 1e-5) continue; // Is zero
        if ($name == 'RATE_GLOBAL_L0') {
          $L0Rate = number_format( $rate );
          continue;
        }
        if ($name == 'RATE_GLOBAL_L1') {
          $L1Rate = number_format( $rate );
          continue;
        }
        if ($name == 'RATE_GLOBAL_HLT') {
          $HLTRate = number_format( $rate );
          continue;
        }
        if ($name == 'RATE_GLOBAL_PHYSICS' || $name == 'STREAM:Main') {
          $PhysicsRate = number_format( $rate );
          continue;
        }
        $groupRates[str_ireplace("RATE_","",$name)] = $rate;
      }
    }
    //print_r($groupRates);
    if (count($groupRates) == 0) return false;
    asort($groupRates); //Sort into order
echo <<< PI_OUT
<script type="text/javascript">
// <![CDATA[

  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Group', 'Rate [Hz]', {role: 'style'}],

PI_OUT;
    $keys = array_keys($groupRates);
    $lastKey = end($keys);
    foreach ($groupRates as $name => $rate) {
      $maxSliceRate = 250.;
      $fractionOfMax = $rate / $maxSliceRate;
      if ($fractionOfMax > 1.) $fractionOfMax = 1.;
      $fractionOfMax = intval($fractionOfMax * 255);
      $aStr = str_pad( base_convert($fractionOfMax,       10, 16), 2, '0', STR_PAD_LEFT);
      $bStr = str_pad( base_convert(255 - $fractionOfMax, 10, 16), 2, '0', STR_PAD_LEFT);
      $colourStr = '#' . $aStr . '00' . $bStr;
      if ( strpos($this->tag, "13TeV") !== FALSE) $colourStr = '#' . $aStr . $bStr . '00';
      echo "      ['{$name}', {$rate}, '{$colourStr}']";
      if ( $name != $lastKey ) echo ",";
      echo "\n";
    }
    if ($L0Rate > 0) $title = "Global Rate L0: {$L0Rate} Hz, L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz";
    else if ($PhysicsRate > 0) $title = "Global Rate L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz, Physics: {$PhysicsRate} Hz";
    else $title = "Global Rate L1: {$L1Rate} Hz, HLT: {$HLTRate} Hz";
echo <<< PI_OUT
    ]);


    var options = {
      fontSize: 10,
      fontName: 'Helvetica',
      chartArea: {'width': '75%', 'height': '75%'},
      title: '{$title}',
      legend: { position: 'none' },
      bar: {groupWidth: '50%'},
      vAxis: {logScale: true, title: 'Rate [Hz]' },
      hAxis: {allowContainerBoundaryTextCufoff: true}
    };

    var chart = new google.visualization.ColumnChart(document.getElementById("chart_values"));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {
      document.getElementById('pngLink').outerHTML = '<a href="' + chart.getImageURI() + '">Link to Graph</a>';
    });

    chart.draw(data, options);
  }

// ]]>    
</script>
<br/>
<div id='pngLink'></div>
<div id="chart_values" style="width: 1200px; height: 700px; display: block; margin: 0 auto;"></div>
PI_OUT;
  }

  function insertCPUPiChart() {
    // We are going to call the table generation script inline!
    // This will get us the data we can parse to make a pi chart
    $_GET['rp'] = 500; // Results to return
    $_GET['query'] = false; //no search
    $_GET['qtype'] = -1; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $this->csvFile;
    $_GET['inline'] = TRUE;
    // Get the data
    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result = json_decode($jsonResult)->{"rows"}; // Convert to array - get data (only one entry)
    if (count($result) == 0) return false; // No data to display
    $cpuTable = array();

    // Need to locate the correct column
    $arrayLocation = array_search("Alg Total Time [%]", $this->getHeaderArray());
    if ($arrayLocation === FALSE) $arrayLocation = array_search("Group Total Time [%]", $this->getHeaderArray());
    foreach($result as $row) {
      foreach($row as $key => $data) {
        if ($key == 'id') continue;
        $name = $data[0];
        $name = str_ireplace("BW_","",$name);
        $name = str_ireplace("BW:","",$name);
        $cpu = $data[  $arrayLocation ];
        $cpuTable[ $name ] = $cpu;
      }
    }
    if (count($cpuTable) == 0) return false;
    arsort($cpuTable); //Sort into order
echo <<< PI_OUT
<script type="text/javascript">
// <![CDATA[

  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Group', 'CPU Usage [%]', {role: 'style'}],

PI_OUT;
    $lastKey = end(array_keys($cpuTable));
    foreach ($cpuTable as $name => $cpu) {
      $fractionOfMax = $cpu / 50.;
      if ($fractionOfMax > 1.) $fractionOfMax = 1.;
      $fractionOfMax = intval($fractionOfMax * 255);
      $aStr = str_pad( base_convert($fractionOfMax,       10, 16), 2, '0', STR_PAD_LEFT);
      $bStr = str_pad( base_convert(255 - $fractionOfMax, 10, 16), 2, '0', STR_PAD_LEFT);
      $colourStr = '#' . 'ff' .  $bStr . '00';
      echo "      ['{$name}', {$cpu}, '{$colourStr}']";
      if ( $name != $lastKey ) echo ",";
      echo "\n";
    }
    $title = "CPU Usage Per Slice";
echo <<< PI_OUT
    ]);


    var options = {
      fontSize: 14,
      fontName: 'Helvetica',
      chartArea: {'width': '65%', 'height': '80%'},
      title: '{$title}',
      legend: { position: 'none' },
      bar: {groupWidth: '50%'},
      hAxis: {title: 'CPU Usage [%]' },
      vAxis: {allowContainerBoundaryTextCufoff: true}
    };

    var chart = new google.visualization.BarChart(document.getElementById("chart_values"));

    // Wait for the chart to finish drawing before calling the getImageURI() method.
    google.visualization.events.addListener(chart, 'ready', function () {
      document.getElementById('pngLink').outerHTML = '<a href="' + chart.getImageURI() + '">Link to Graph</a>';
    });

    chart.draw(data, options);
  }

// ]]>    
</script>
<div id="chart_values" style="width: 700px; height: 600px; display: block; margin: 0 auto;"></div>
<div id='pngLink'></div>
PI_OUT;
  }
  
  
  function insertItemDetail() {
    // We are going to call the table generation script inline!
    // Manually set the required GETs
    $_GET['rp'] = 1; // Results to return
    $_GET['query'] = $this->item;
    $_GET['qtype'] = 0; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $this->csvFile;
    $_GET['inline'] = TRUE;
    // Get the data
    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    if (count(json_decode($jsonResult)->{"rows"}) == 0) return false; // No data to display
    $result = json_decode($jsonResult)->{"rows"}[0]->cell; // Convert to array - get data (only one entry)
    $headerArray = $this->getHeaderArray();
    $tooltipArray = $this->getFooterArray();
    echo "<div class='columnClear'><div class='resultDisplay'><table>\n";
    foreach ($headerArray as $key => &$header) {
      if ($key == 0) echo "<tr><td class='rightText'>Summary Property</td><td>Value</td></tr>\n";
      echo "<tr><td class='rightText' title='$tooltipArray[$key]'>{$header}</td><td>$result[$key]</td></tr>\n";
    }
    echo "</table></div></div>";
    return true;
  }
  
  function insertAlgLinkTable($algs) {
    // We are going to call the table generation script inline!
    // Manually set the required GETs
    $linkedSummary = $this->summary . "_Algorithm";
    // Want to get the CSV not for this page (e.g. chain) but for the chain's als page
    $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $linkedSummary);

    // If Chain_Algorithm summary (mode 1) is missing, use algorithm summary (mode 2)
    if(!file_exists($checkCSV)){
      $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, "Algorithm");
      $mode = 2;
    } else {
      $mode = 1;
    }

    $_GET['rp'] = 100000; // Results to return
    $_GET['query'] = false;
    $_GET['qtype'] = -1; // Column to search
    $_GET['dir'] = $this->dir; 
    $_GET['type'] = $this->type;
    $_GET['tag'] = $this->tag;     
    $_GET['run'] = $this->run; 
    $_GET['csvfile'] = $checkCSV;
    $_GET['inline'] = TRUE;

    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result = json_decode($jsonResult, true);
    $rows = $result['rows'];

    $checkCSV = getCsv($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, "Algorithm");
    $_GET['csvfile'] = $checkCSV;

    ob_start(); // Start buffer to get result from request
    require("csv-to-json.php"); //execute request
    $jsonResult = ob_get_clean(); // Get result (json formatted string)
    $result_all = json_decode($jsonResult, true);
    $rows_all = $result_all['rows'];

    global $separator;
    echo "<div class='columnClear'><div class='resultDisplay'><table>\n";
    echo "<tr><td class='centerText' style='font-weight:bold'>{$this->summary} Algorithms</td></tr>\n";
    if (count($algs) == 0) { // No Algorithms to display!
      echo "<tr><td class='centerText'>None</td></tr>\n";
    } else {

      foreach ($algs as $key => &$alg) {
        // Check if we actually have any data for this alg
        $foundData = false;
        $myCalls = 0;
        if (isset($rows)) {
          foreach ($rows as $row) {
            $cell = $row['cell'];
            $name = $cell[0];
            $myCalls = $cell[2];
            $checkName = $this->item . $separator . $alg;
            // echo "check $checkName vs $name</br>";
            if ($checkName == $name) {
              $foundData = true;
              break;
            }
          }
        }

        // Check if we have all-chain data for this alg
        $foundAnyData = false;
        $allCalls = 0; 
        if (isset($rows_all)) {
          foreach ($rows_all as $row) {
            $cell = $row['cell'];
            $name = $cell[0];
            $allCalls = $cell[2];
            // echo "check $checkName vs $name</br>";
            if ($mode == 1){
              $algName = $alg;
            } else {
              $algName = explode("/", $alg)[1];
            }
            if ($algName == $name) {
              $foundAnyData = true;
              break;
            }
          }
        }

        $linkedItem = $this->item . $separator . $alg;
        $urlThisChain = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $linkedSummary, $linkedItem);

        if ($mode == 1) {
          $urlAllChains = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, 'Algorithm', $alg);
        } else {
          // For example T2CaloEgammaReFastAlgo/FastCaloL2EgammaAlg -> FastCaloL2EgammaAlg
          $urlAllChains = getLinkItem($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, 'Algorithm', explode("/", $alg)[1]);
        }

        echo "<tr><td class='centerText'>{$key}. {$alg}";
        if ($foundAnyData && $this->summary == 'Chain') {
          echo " <a href='{$urlAllChains}'>[AllChains calls:{$allCalls}]</a>";
        } else if ($foundAnyData && $this->summary == 'Sequence') {
          echo " <a href='{$urlAllChains}'>[AllSequences calls:{$allCalls}]</a>";
        }
        if ($foundData && $this->summary == 'Chain') {
          echo " <a href='{$urlThisChain}'>[ThisChain calls:{$myCalls}]</a>";
        } else if ($foundData && $this->summary == 'Sequence') {
          echo " <a href='{$urlThisChain}'>[ThisSequence calls:{$myCalls}]</a>";
        }
      }
    }

    echo "</table></div></div>\n";
  }

  function insertTable() {
    // Get just the first line from the CSV to get the column names
    $headerArray = $this->getHeaderArray();
    $tooltipArray = $this->getFooterArray();
    $columnSize = count($headerArray);
    if (strpos($this->summary, "Rate_Chain") !== FALSE) {
      if (checkXML($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary) !== 0) {
        $rateFile = getXML($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary);
        $rateFileB = getXML($this->dirB, $this->typeB, $this->tagB, $this->runB, $this->rangeB, $this->levelB, $this->summaryB);
      } else if (checkJSON($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary) !== 0){
        $rateFile = getJSON($this->dir, $this->type, $this->tag, $this->run, $this->range, $this->level, $this->summary);
        $rateFileB = getJSON($this->dirB, $this->typeB, $this->tagB, $this->runB, $this->rangeB, $this->levelB, $this->summaryB);
      }
      if (isset($rateFile)){
        $strA = "";
        if ($this->doComp == true) $strA = " A";
        echo "<p class='linkToData'>File of rates{$strA}: <a href='{$rateFile}'>{$rateFile}</a></p>";
        if ($this->doComp == true && isset($rateFileB)) {
          echo "<p class='linkToData'>File of rates B: <a href='{$rateFileB}'>{$rateFileB}</a></p>";
        }
      }
    }
    if ($this->doComp == false) {
      echo "<p class='linkToData'>Table parsed from CSV file: <a href='{$this->csvFile}'>{$this->csvFile}</a></p>";
    }
echo <<< TABLE_OUT
<table class="{$this->tableName}" style="display: none"><tr><td></td></tr></table>
<script type="text/javascript">
// <![CDATA[

  var doOnce = 0;
	var tableCallback = function(table) { 
	  if (doOnce == 1) return;
	  doOnce = 1;
	  
    var nColumns = {$columnSize};
    console.log(table);
    //table.autoResizeColumn(table);
    // Issue a resize command to all colls
    for (var n = 1; n < nColumns; ++n) {
      console.log("resize col " + n);
    
		  //var n = $('div', this.cDrag).index(obj),
			var \$th = $('th:visible div:eq(' + n + ')', table.hDiv),
			  ol = 100; //parseInt(obj.style.left, 10),
			  ow = \$th.width(),
			  nw = 0,
			  nl = 0,
			 \$span = $('<span />');
		  $('body').children(':first').before(\$span);
		 \$span.html(\$th.html());
		 \$span.css('font-size', '' + \$th.css('font-size'));
		 \$span.css('padding-left', '' + \$th.css('padding-left'));
		 \$span.css('padding-right', '' + \$th.css('padding-right'));
		  nw = \$span.width();
		  $('tr', table.bDiv).each(function () {
			  var \$tdDiv = $('td:visible div:eq(' + n + ')', table),
				  spanW = 0;
			 \$span.html(\$tdDiv.html());
			 \$span.css('font-size', '' + \$tdDiv.css('font-size'));
			 \$span.css('padding-left', '' + \$tdDiv.css('padding-left'));
			 \$span.css('padding-right', '' + \$tdDiv.css('padding-right'));
			  spanW = \$span.width();
			  nw = (spanW > nw) ? spanW : nw;
		  });
		 \$span.remove();
		  //nw = (table.minWidth > nw) ? table.p.minWidth : nw;
		  nl = ol + (nw - ow);
		  $('div:eq(' + n + ')', table.cDrag).css('left', nl);
		  table.colresize = {
			  nw: nw,
			  n: n
		  };
		  table.dragEnd();
    
    }
	}
	
  $('.$this->tableName').flexigrid({
    url : 'csv-to-json.php',
    method: 'GET',
    params: [
      {
        name: 'dir',
        value:'{$this->dir}'
      }, {
        name: 'type',
        value:'{$this->type}'
      }, {
        name: 'tag',
        value:'{$this->tag}'
      }, {
        name: 'run',
        value:'{$this->run}'
      }, {
        name: 'range',
        value:'{$this->range}'
      }, {
        name: 'level',
        value:'{$this->level}'
      }, {
        name: 'summary',
        value:'{$this->summary}'
      }, {
        name: 'csvfile',
        value:'{$this->csvFile}'
      }
TABLE_OUT;
if ($this->doComp == true) {
echo <<< TABLE_COMPARISON
      , {
        name: 'dirB',
        value:'{$this->dirB}'
      }, {
        name: 'typeB',
        value:'{$this->typeB}'
      }, {
        name: 'tagB',
        value:'{$this->tagB}'
      }, {
        name: 'runB',
        value:'{$this->runB}'
      }, {
        name: 'rangeB',
        value:'{$this->rangeB}'
      }, {
        name: 'levelB',
        value:'{$this->levelB}'
      }, {
        name: 'summaryB',
        value:'{$this->summaryB}'
      }, {
        name: 'csvfileB',
        value:'{$this->csvFileB}'
      }, {
        name: 'compLumi',
        value:'{$this->compLumi}'
      }, {
        name: 'lumiA',
        value:'{$this->lumiA}'
      }, {
        name: 'lumiB',
        value:'{$this->lumiB}'
      }, {
        name: 'doComp',
        value:'1'
      }
TABLE_COMPARISON;
} 
echo <<< TABLE_OUT
    ],
    dataType : 'json',
    colModel : [

TABLE_OUT;
    // DEFINE COLUMN HEADERS
    foreach($headerArray AS $key => &$column) {
      if ($key == 0) {
        echo "      {\n";
      } else {
        echo "      }, {\n";
      }
      echo "        display : '$column',\n";
      echo "        name: $key,\n";
      echo "        tooltip: '$tooltipArray[$key]',\n";
      echo "        sortable : true,\n";
      if (isset($this->toHide[$key]) && $this->toHide[$key] == true) {
        echo "        hide : true,\n";
      }
      if ($key == 0) {
        echo "        width : 225,\n";
        echo "        align : 'left'\n";
      } else {
        echo "        width : 100,\n";
        echo "        align : 'center'\n";
      }
    }
echo <<< TABLE_OUT
    } ],
    searchitems : [ 

TABLE_OUT;
    // DEFINE WHAT CAN BE SEARCHED
    foreach($this->searchable as $key => &$entry) {
      if (array_key_exists($key, $headerArray) === FALSE) continue;
      if ($key == 0) {
        echo "      {\n";
      } else {
        echo "      }, {\n";
      }
      echo "        display : '$headerArray[$key]',\n";
      echo "        name: {$key},\n";
      if ($key == 0) {
        echo "        isdefault : true,\n";
      } else {
        echo "        isdefault : false,\n";
      }
    }
echo <<< TABLE_OUT
    } ],
    sortname : {$this->sortCol},
    sortorder : "{$this->sortDir}",
    usepager : true,
    title : '{$this->tableTitle}',
    useRp : true,
    rp : 50,
    rpOptions: [10, 50, 100, 200, 500, 1000, 5000],
    showTableToggleBtn : true,
    width : 'auto',
    dblClickResize : true,
    height : 'auto',
    onSuccess : tableCallback
  });
	

  
// ]]>    
</script>
TABLE_OUT;
  }
  
}
?>
