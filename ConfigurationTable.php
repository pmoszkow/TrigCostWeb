<?php
class ConfigurationTable {

  var $configFile;
  var $configType;
  var $search;
  
  function __construct($configFile, $configType="xml") {
    $this->configFile = $configFile;
    $this->configType = $configType;
  }
  
  function setSearch($search) {
    $this->search = $search;
  }
  
  function getChildren($currentLevel) {
    foreach($currentLevel as $key => $val) {
      if ($key == 'children') {
        return $val;
      }    
    }
  }
  
  function getName($currentLevel) {
    foreach($currentLevel as $key => $val) {
      if ($key == 'text') {
        return $val;
      }    
    }
  }
  
  function getChainAlgsInternal($chainArray) {
    $chainAlgs = array();
    $sigs = $this->getChildren($chainArray);
    foreach ($sigs as &$sig) {
      $seqs = $this->getChildren($sig);
      foreach ($seqs as &$seq) {
        $algs = $this->getChildren($seq);
        foreach ($algs as &$alg) {
          $chainAlgs[] = $this->getName($alg);
        }
      }
    }
    return array_unique( $chainAlgs );
  }

  function getChainAlgsPerSequences($seqs, $allSeqs) {
    $chainAlgs = array();
    foreach( $seqs as $seq) {
      $chainAlgs += $allSeqs[$seq];
    }
    return array_unique( $chainAlgs );
  }
  
  function getChainAlgs($chainName) {
    if ($this->configType == "json"){
      $runMetadata = json_decode ( file_get_contents($this->configFile), true );
      foreach( $runMetadata["children"] as $mdEntry) {
        foreach( $mdEntry as $key => $value) {
          if ($key != "HLTMenu") continue;
          $confArr = $value;
        }
      }
    } else {
      $data = file_get_contents( $this->configFile );
      $confArr = json_decode($data, true);
    }
    
    if ( !isset($confArr) || $confArr == "") {
      return array();
    }

    if ($this->configType == "json"){
      foreach ($confArr["chains"] as $key => $value) {
        // Check if the *start* of the names match (we might be adding extra junk e.g. prescales to the end)
        if (0 === strpos($key, $chainName)) {
          // Remove empty steps
          $sequencers = array_filter($value["sequencers"]);
          return $this->getChainAlgsPerSequences($sequencers, $confArr["sequencers"]);
        }
      }
    } else {
      $trigger = $this->getChildren( $confArr );
      $hlt = $this->getChildren( $trigger[1] );
      foreach ($hlt as &$hltChain) {
        if (0 === strpos($this->getName($hltChain), $chainName)) {
          return $this->getChainAlgsInternal($hltChain);
        }
      }
    }

    return array();
  }
  

  function getSeqAlgs($seqName) {
    $data = file_get_contents( $this->configFile );
    $confArr = json_decode($data, true);
    if ($confArr == "") return array();
    $trigger = $this->getChildren( $confArr );
    $hlt = $this->getChildren( $trigger[1] );
    
    $seqAlgs = array();
    foreach ($hlt as &$chain) {
      $sigs = $this->getChildren($chain);
      foreach ($sigs as &$sig) {
        $seqs = $this->getChildren($sig);
        foreach ($seqs as &$seq) {
          //if ($seqName != $this->getName($seq)) continue;
          if (!(0 === strpos($this->getName($seq), $seqName))) continue; // Like with chain, check start of name
          $algs = $this->getChildren($seq);
          foreach ($algs as &$alg) {
            $seqAlgs[] = $this->getName($alg);
          }
        }
      }
    }
    return array_unique( $seqAlgs );
  }


  function insertConfigTable() {
    global $dir, $type, $tag, $run, $config;
    $data = file_get_contents( $this->configFile );
  
echo <<< EXPORT

<p>
<input type="text" id="searchBox" value="{$this->search}" class="largerInput" /><button id='searchButton'>Search</button><button id='clearButton'>Reset</button>
</p>
<div id="confTable"></div>
<script>
// <![CDATA[

$("#searchBox").keyup(function(event){
  if(event.keyCode == 13){ //Enter
    $("#searchButton").click();
  }
});

$(function () {
	
	// Pass search to tree
	var to = false;
	$('#searchButton').on('click', function () {
		if(to) { clearTimeout(to); }
		to = setTimeout(function () {
			var v = $('#searchBox').val();
			$('#confTable').jstree(true).search(v);
		}, 250);
	});
	
	$('#clearButton').on('click', function () {
	  $('#searchBox').val("");
	  $('#confTable').jstree(true).search("");
	});
	
	// Enable all sub-nodes of a search
	enableSubtree = function(elem) {
	  // was ("ul:first")
	  elem.siblings("ul").find("li").show();
	  return correctNode(elem.siblings("ul"));
	};
	 
	correctNode = function(elem) {
	  var child, children, last, _j, _len1, _results;
	  last = elem.children("li").eq(-1);
	  last.addClass("jstree-last");
	  children = elem.children("li");
	  //console.log(children);
	  _results = [];
	  for (_j = 0, _len1 = children.length; _j < _len1; _j++) {
		child = children[_j];
		_results.push(correctNode($(child).children("ul:first")));
	  }
	  return _results;
	};

	$("#confTable").jstree({
	  'core' : {
      'data' : [
        {$data}
      ]
    },
    'search' : { 'fuzzy' : false, 'show_only_matches': true },
		"plugins" : [ "search" ]
	}).bind("select_node.jstree", function (e, data) {
		console.log(data);
		// Get if chain node
		if (data.node.icon == 'images/C.png') { // I'm a chain
		  if(data.node.state.opened == true) {
		    data.instance.close_all(data.node);
		  } else {
		    data.instance.open_all(data.node);
		  }
		} else {
		  data.instance.toggle_node(data.node);
		}
		if (data.node.icon == 'images/A.png') { //I'm an alg
		  // TODO HLT is currently hardcoded in here
		  var goToUrl = "?dir={$dir}&type={$type}&tag={$tag}&run={$run}&type={$type}&range={$config}&level=HLT&summary=Algorithm&item=" + data.node.text;
		  console.log("Clicked an ALG, goto " + goToUrl );
      window.location.href = goToUrl;
		}
    enableSubtree ($(".jstree-search"));		
	}).bind("search.jstree", function (e, data) {
		// Show all sub-nodes
		data.instance.open_all( data.nodes.children() );
		enableSubtree ($(".jstree-search"));
	}).bind("loaded.jstree", function (e, data) {
		if ( $('#searchBox').val() != "" ) {
		  $("#searchButton").click(); // auto search
    }
	});
	
});

// ]]>
</script>
EXPORT;
  }
  
}
?>
