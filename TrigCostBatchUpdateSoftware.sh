#!/bin/bash

# This script checks that up to date versions of the required software are installed and will update them if not.
# The initial install is handled by the puppet configuration which has hard coded versions

#echo "Update SW] Checking if SW needs updating"

echo "THIS IS DEPRICATED FOR NOW - USE RELEASE"

exit

export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh

source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh git

################################################## ##################################################
HOME=/opt/trig-cost-sw
V_BRANCH=release/21.1.7
################################################## ##################################################

# Authenticate
kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab

# Check packages 
cd $HOME/athena
C_BRANCH=`git rev-parse --abbrev-ref HEAD` # This returns just the tag
if [[ "$C_BRANCH" != "$V_BRANCH" ]]; then
  echo "Update SW] Updating from $C_BRANCH to $V_BRANCH"
  cd $HOME/athena
  git checkout $V_BRANCH
  REBUILD=1
fi

if [ -z "$REBUILD" ]; then
  #echo "Update SW] All up to date"
  exit
fi

echo "Update SW] Recompiling"
cd $HOME
source ${AtlasSetup}/scripts/asetup.sh AthenaP1,21.1,latest
cd $HOME/build

echo "Running cmake"
cmake ../athena/Projects/WorkDir
echo "Compile packages"
make -j8
echo "Update SW] Finished Recompiling"

exit


## Initial setup

#HOME=/opt/trig-cost-sw
#cd $HOME
#export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/
#source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#source $ATLAS_LOCAL_ROOT_BASE/packageSetups/localSetup.sh git
#kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab

##must be run from attradm
#git atlas init-workdir https://:@gitlab.cern.ch:8443/atlas/athena.git

#cd athena
#git atlas addpkg TrigCostRootAnalysis
#git atlas addpkg TrigRootAnalysis
#git atlas addpkg TrigCostD3PD
