<?php 

function sanitise($string) {
  return htmlspecialchars( stripslashes( trim($string) ) );
}

function getVersionFromCMTPATH($value) {
    $string = " ".$value;
    $ini = strpos($string,"Atlas");
    if ($ini == 0) return "";
    $len = strpos($string,":",$ini) - $ini;
    return substr($string,$ini,$len);
}

function getDetailLevel() {
  global $page, $dir, $type, $tag, $run, $range, $config, $level, $summary, $item, $err;
  
  $DISPLAY_LEVEL = 0;
  
  // Check special page values
  if ( isset($page) && $page != 'metadata') {
    $DISPLAY_LEVEL = 100;
    return $DISPLAY_LEVEL;
  }

  // Check dir level
  if ( isset($dir)) {
    if (!is_link($dir) ){
      $err[] = "ERROR: Cannot find data for Dir={$dir}.";
    } else {
      $DISPLAY_LEVEL = 1; //Dir level
    }
  }
  
  // Check run level
  if ( empty($err) && isset($dir) && isset($tag) && isset($run) ) {
    // See if we have the CSV
    if ( checkRun($dir, $type, $tag, $run) == 0 ) {
      $err[] = "ERROR: Cannot find data for Dir={$dir}, Type={$type}, Tag={$tag}, Run={$run}.";
    } else {
      $DISPLAY_LEVEL = 2; //Run level
    }
  } 
  
  // Cannot have config and range
  if ( empty($err) && isset($range) && isset($config) ) {
    $err[] = "ERROR: Please supply only one of 'config' or 'range' options";
  }
    
  // Check trig conf level
  if ( empty($err) && isset($config) ) {
    // See if we have the CSV
    if ( checkConfig($dir, $type, $tag, $run, $config) == 0 ) {
      $err[] = "ERROR: Cannot find trigger configuration for Config={$config}";
    } else {
      $DISPLAY_LEVEL = 3; // Config level
    }
  } 
  
  // Check range level
  if ( empty($err) && isset($range) ) {
    // See if we have CSVs
    if ( checkCsv($dir, $type, $tag, $run, $range) == 0 ) {
      $err[] = "ERROR: Cannot find any tabular data for Range={$range}";
    } else {
      $DISPLAY_LEVEL = 3; // Range level
    }
  } 

  // Check metadata level
  if ( empty($err) && isset($page) && $page == 'metadata' ) {
    // See if we have CSVs
    if ( checkMetadata($dir, $type, $tag, $run) == 0 ) {
      $err[] = "ERROR: Cannot find any metadata for run={$run}";
    } else {
      $DISPLAY_LEVEL = 3; // Range level
    }
  } 
  
  // Check item detail and table summary level
  if ( empty($err) && (isset($level) || isset($summary)) ) {
    // Both must be set
    // Check that level and summary were both set
    if ( !isset($level) || !isset($summary) ) {
      $err[] = "ERROR: Both level and summary need to be supplied to display data tables or item summaries.";
    }
  }
  
  // Check item detail and table summary level
  if ( empty($err) && isset($level) && isset($summary) ) {
    // See if we have the CSV
    if ( checkCsv($dir, $type, $tag, $run, $range, $level, $summary) == 0 ) {
      $err[] = "ERROR: Cannot find data for Level={$level}, Summary={$summary}";
    } else {
      $DISPLAY_LEVEL = 4;
    }
  }
  
  // Check item level
  if ( empty($err) && isset($item) ) {
    // We used to check here if there was a ROOT file. But we don't actually need to.
    $DISPLAY_LEVEL = 5;
  }
  
  return $DISPLAY_LEVEL;
}

function echoErrorMsgs() {
  global $err;

  if(!empty($err))  {
    echo "<div align='center'>\n";
    //echo "<p>\n";
    foreach ($err as $e) {
      echo "<div class='err'>\n";
      echo "$e <br/>\n";
      echo "</div>\n";   
      unset($err); 
    }
    //echo "</p>\n";
    echo "</div>\n";
  }
}

$start = 0.;
function echoPageHeader($title) {
  date_default_timezone_set('Europe/Zurich');
  global $config, $strictCSS, $start, $item, $summary, $page, $isDark;
  $time = explode(' ', microtime(TRUE));
  $start = $time[0];
echo <<< EXPORT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <link rel="shortcut icon" href="favicon.ico"/> 
    <title>{$title}</title>
    <script type="text/javascript" src="scripts/jquery.min.js"></script>
    <script type="text/javascript" src="scripts/jquery-ui.min.js"></script>	 

EXPORT;
  if ( isset($config) ) {
    $strictCSS = FALSE; //:(
    echo "    <script type='text/javascript' src='scripts/jstree.min.js'></script>\n"; // For conf table
    echo "    <link rel='stylesheet' type='text/css' href='style/jstree.min.css' />\n"; // For conf table
  }
  if ( isset($item) ) {
    echo "    <script type='text/javascript' src='scripts/JSRoot.core.js'></script>\n"; 
    echo "    <script type='text/javascript' src='scripts/d3.min.js'></script>\n"; // For D3 display of trig data
    echo "    <link rel='stylesheet' type='text/css' href='style/d3.css' />\n"; // For data tables
    $strictCSS = FALSE; //:(
  }
  if ( isset($summary) or (isset($page) and $page == "CompareRates") ) {
    echo "    <script type='text/javascript' src='scripts/flexigrid.js'></script>\n"; // For data tables
    echo "    <link rel='stylesheet' type='text/css' href='flexigrid.pack.css' />\n"; // For data tables
  }
  if ( isset($summary) and ($summary == 'Rate_Group' or $summary == 'Rate_Upgrade_Group' or $summary == 'SliceCPU')) {
    echo "    <!--Load the GOOGLE GRAPH AJAX API-->\n";
    echo "    <script type='text/javascript' src='https://www.google.com/jsapi'></script>\n";
  }
  if ($isDark) {
    echo     "<link rel='stylesheet' type='text/css' href='style/style_dark.css' />\n";
  } else {
    echo     "<link rel='stylesheet' type='text/css' href='style/style.css' />\n";
  }
echo <<< EXPORT
    <script type='text/javascript'>
    // <![CDATA[
    $(function(){
        $('a.linkExternal').on('click',function(e){
            e.preventDefault();
            window.open($(this).attr('href'));
        });
    });
    // ]]>
    </script>
</head>
<body>
EXPORT;
}

function echoFooter() {
  global $strictXHTML, $strictCSS, $start;
  echo "<div class='clearBoth'></div><hr/>\n";
  if ($strictXHTML === TRUE || $strictCSS === TRUE) {
    echo "<p>\n";
    if ($strictXHTML === TRUE) {
      echo "<a href='http://validator.w3.org/check?uri=referer'><img src='http://www.w3.org/Icons/valid-xhtml10-blue' alt='Valid XHTML 1.0 Strict' height='31' width='88' /></a>";
    }
    if ($strictCSS === TRUE) {
      echo "<a href='http://jigsaw.w3.org/css-validator/check/referer'>";
      echo "<img style='border:0;width:88px;height:31px' src='http://jigsaw.w3.org/css-validator/images/vcss-blue' alt='Valid CSS!' /></a>";
    }
    echo "</p>\n";
    $time = explode(' ', microtime(TRUE));
    $finish = $time[0];
    $total_time = round(($finish - $start), 4);
    echo "<p>Page generated in {$total_time}s. <a href='" . getLinkPage('About') . "'>About the Trig Cost Browser</a></p>";
  }
echo <<< EXPORT
</body>
</html>
EXPORT;
}

function echoBreadcrumb() {
  global $detailLevel, $dir, $type, $tag, $run, $range, $rangeDisplay, $config, $configDisplay, $search, $level, $summary, $summaryDisplay, $item, $isAdmin, $isExpert, $page;
  echo "<p>\n";
  echo "<a href='.'>Home</a>\n";
  while (true) {
    
    // Special case 1
    if ( $detailLevel == 100 ) {
      echoRArrow();
      echo "<a href='" . getLinkPage($page) . "'>Page: {$page}</a>\n";
      break;
    }

    if ( $detailLevel < 1 ) break; 
    echoRArrow();
    echo "<a href='" . getLinkDir($dir) . "'>Directory: {$dir}</a>\n";
  
    if ( $detailLevel < 2 ) break; 
    echoRArrow();
    echo "<a href='" . getLinkRun($dir,$type,$tag,$run) . "'>Run: {$type} {$tag} {$run}</a>\n";
    
    if ( $detailLevel < 3 ) break;
    echoRArrow();
    
    if ( isset($range) ) {
      if (strpos($range, 'Event') === FALSE) {
        echo "<a href='" . getLinkRange($dir,$type,$tag,$run,$range) . "'>Range: {$rangeDisplay}</a>\n";
      } else {
        echo "Range: $rangeDisplay";
      }
    } else if ( isset($config) ) {
      echo "<a href='" . getLinkConfig($dir,$type,$tag,$run,$config) . "'>Configuration: {$configDisplay}</a>\n";
    } else if ( isset($page) && $page == 'metadata' ) {
      echo "<a href='" . getLinkMetadata($dir, $type, $tag, $run) . "'>Full Metadata</a>\n";
    } else {
      assert(false);
    }
    
    if ( isset($config) && isset($search) ) {
      echoRArrow();
      echo "<a href='" . getLinkConfigSearch($dir,$type,$tag,$run,$config,$search) . "'>Search: {$search}</a>\n";
    }
    
    if ( $detailLevel < 4 ) break;
    echoRArrow();
    echo "<a href='" . getLinkSummary($dir,$type,$tag,$run,$range,$level,$summary) . "'>Summary: {$level} {$summaryDisplay}</a>\n";
    
    if ( $detailLevel < 5 ) break;
    echoRArrow();
    echo "<a href='" . getLinkItem($dir,$type,$tag,$run,$range,$level,$summary,$item) . "'>Item: {$item}</a>\n";
    break;
  }
  echo "</p>\n";
  if ($isAdmin == TRUE) {
    echo "<p>Admin: <a href='" . getLinkPage('ProcessingRequest') . "'>[New Processing Request]</a> <a href='" . getLinkPage('symlinks') . "'>[Install Links]</a> <a href='" . getLinkPage('log') . "'>[View P1 Logs]</a> <a href='" . getLinkPage('logManual') . "'>[View Request Logs]</a> <a href='" . getLinkPage('sw') . "'>[View Installed SW]</a></p>";
  } else if ($isExpert == TRUE) {
    echo "<p>Trigger Expert: <a href='" . getLinkPage('ProcessingRequest') . "'>[New Processing Request]</a> <a href='" . getLinkPage('log') . "'>[View P1 Logs]</a> <a href='" . getLinkPage('logManual') . "'>[View Request Logs]</a> <a href='" . getLinkPage('sw') . "'>[View Installed SW]</a></p>";
  }
  //$motd_data = file("motd.txt");
  //$motd_line = $motd_data[count($motd_data)-1];
  //if ($motd_line != "") echo "<p><strong>MOTD</strong> <i>" . date ("F d Y", filemtime("motd.txt")) . "</i>: {$motd_line}</p>";
  return;
}


function echoHeader($level, $string) {
  if ($level == 1 && strpos($_SERVER['SERVER_NAME'],'aiatlas105') !== false) {
    $string .= " <span class='processing'>{Dev Server} <a href='https://atlas-trig-cost-test.cern.ch'>[Deploy]</a> <a href='https://atlas-trig-cost.cern.ch/'>[Live]</a></span>";
  }
  if ($level == 1 && strpos($_SERVER['SERVER_NAME'],'aiatlas106') !== false) {
    $string .= " <span class='blueText'>{Deploy Server} <a href='https://atlas-trig-cost-dev.cern.ch'>[Dev]</a> <a href='https://atlas-trig-cost.cern.ch/'>[Live]</a></span>";
  }

  echo "<h{$level}>{$string}</h{$level}>\n";
  
}

function echoRArrow() {
  echo "<span class='rarrow'>&#10137;</span>";
}

function getSubstitutedUrl($dir, $type, $tag, $run, $doSubstitution = true) {
  global $dirPostPatters;
  $theSubDir = "";
  if ( isset( $dirPostPatters[$dir]) ) {
    $types = $dirPostPatters[$dir];
    if ( isset( $types[$type]) ) {
      $theSubDir = $types[$type];
    } 
  }
  if ($doSubstitution == false) return $theSubDir;
  // For now assume $tag in the form rel0 - rel6
  // If tag in the form "rel_X"
  if (strpos($tag,'rel') !== false) {
    $lastChar = substr($tag, -1);
    return str_replace("rel_*", "rel_".$lastChar, $theSubDir);
  } else { // assume i'm of form 2017-04-02T2135 
    // try all possibilities
    for ($t = 0; $t < 7; ++$t) {
      // Check sub-dir exists
      $tStr = (string) $t;
      $testSubDir = str_replace("rel_*", "rel_".$tStr, $theSubDir);
      if (file_exists("{$dir}{$testSubDir}/costMonitoring_{$tag}_{$run}")) {
        return $testSubDir;
      }
    }
    return $theSubDir;
  }
}

function echoTagRunDetails($availableData, $theDirectory, $theType = "") {
  if (count($availableData) == 0) {
    echo "<p><em>None Found.</em></p>";
  }

  echo "<div class='resultDisplay' style='max-width:1400px;'><table style='max-width:1400px;'><tr></tr>\n";

  foreach($availableData as $runTagAndNum) {
    $theTag = $runTagAndNum[0];
    $theRun = $runTagAndNum[1];

    if (checkInProgress($theDirectory, $theType, $theTag, $theRun) != false) {
      // This run is still in progress, display its progress data
      $progressData = json_decode ( file_get_contents(getInProgress($theDirectory, $theType, $theTag, $theRun)), true );
      $pd = $progressData["children"];
      
      echo "<tr><td width='43%'><span class='processing'>[Run Currently Processing]</span> Run Number:{$theRun}, Tag:{$theTag}</td><td>\n";
      echo "<div class='subtitle'>Events Processed: {$pd[0]['EventsProcessed']}/{$pd[1]['EventsInFiles']}</div>";
      echo "<div class='subtitle'>Approximate Time Remaining: {$pd[2]['HoursLeft']}h {$pd[3]['MinsLeft']}m</div>";
      echo "</td></tr>";
    } else {
      // Run is not currently processing
      if (checkMetadata($theDirectory, $theType, $theTag, $theRun) != false) {
        $runMetadata = json_decode ( file_get_contents(getMetadata($theDirectory, $theType, $theTag, $theRun)), true );
        $md = $runMetadata["children"];
      } else {
        unset($md);
      }
      echo "<tr><td width='43%'><a href='" . getLinkRun($theDirectory, $theType, $theTag, $theRun) . "'>Run Number:<strong>{$theRun}</strong>, Tag:<strong>{$theTag}</strong></a>";
      echo "  (<a href='http://atlas-runquery.cern.ch/query.py?q=find+run+{$theRun}+/+show+all' class='linkExternal'>Run Query</a>)\n";
      $JIRA = getSingleMetadata($theDirectory, $theType, $theTag, $theRun, "JIRA");
      if ($JIRA != NULL) echo "  (<a href='https://its.cern.ch/jira/browse/{$JIRA}' class='linkExternal'>{$JIRA}</a>)\n";
      echo "</td><td>";
      if (isset($md)) {
        foreach( $md as $mdEntry) {
          foreach( $mdEntry as $key => $value) {
            if (! ($key == "Date of Processing" || $key == "Details") ) continue;
            echo "<div class='subtitle'>{$key}: <strong>{$value}</strong></div>"; 
          }
        }
      }
    }
    echo "</td></tr>";
  }
  echo "</table></div>\n";
} 

function checkSymLinks() {
  global $addLink, $rmLink, $err, $USERNAME, $dataDirs2015, $dataDirs2016, $dataDirs2017, $dataDirs2018, $dataDirs2020, $page, $otherLinks;
  # Remove user link
  if (isset($rmLink)) {
    if (is_link($USERNAME) == false) {
      $err[] = "User {$USERNAME} does not have a linked directory.";
    } else {
      $exLocation = readlink($USERNAME);
      unlink($USERNAME);
      $err[] = "User {$USERNAME}'s personal run folder, <em>\"{$exLocation}\"</em>, has been unlinked from this website.";    
    }
  } 
  # Add user link
  if (isset($addLink)) {
    if (is_link($USERNAME) == true) {
      $err[] = "User {$USERNAME} allready has a linked directory, please unlink the old one before changing to a new one.";
    } else if (is_dir($addLink) == false) {
      $err[] = "Supplied directory \"{$addLink}\" for user {$USERNAME} can not be resolved. Is it on /afs or /cvmfs and publicly readable?";    
    } else {
      symlink($addLink, $USERNAME);
      $err[] = "Linked personal directory <em>\"{$addLink}\"</em> for user {$USERNAME}.";    
    }
  }  
  # Check base links
  reset($dataDirs2015);
  $first_key = key($dataDirs2015);
  // Try to get first folder
  if (!is_link($first_key) || $page == 'symlinks') {
    $err[] = "Fresh install of TrigCostWeb, creating predefined links.";
    // Nope. then we need to form them
    foreach ($dataDirs2015 as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked. Unlinking it first.";
        unlink($key);
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
    foreach ($dataDirs2016 as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked. Unlinking it first.";
        unlink($key);
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
    foreach ($dataDirs2017 as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked. Unlinking it first.";
        unlink($key);
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
    foreach ($dataDirs2018 as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked. Unlinking it first.";
        unlink($key);
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
    foreach ($dataDirs2020 as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked. Unlinking it first.";
        unlink($key);
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
    foreach ($otherLinks as $key => $value) {
      if (is_link($key)) {
        $err[] = "Directory <em>{$key}</em> already linked.";
        continue;
      }
      $err[] = "Linking <em>{$key}</em> to <em>\"{$value}\"</em>";
      symlink($value, $key);
    }
  }
}

function getRuns($directory, $type = "") {
  global $dirPostPatters;
  $subDirectory = getSubstitutedUrl($directory, $type, "", "", /* do substitution */ false);
  $runTagAndNum = array();
  $dirs = array_reverse(glob( "{$directory}{$subDirectory}/costMonitoring_*", GLOB_ONLYDIR));
  foreach( $dirs as &$dir ) {
    $endOfDir = substr( $dir, strrpos( $dir, '/' )+1 ); // Get everything after last "/"
    $explode = explode("_", $endOfDir, 3); // Get the three blocks of text "costMonitoring", "TAG" and "runNumber 
    $runTag = $explode[1];
    // TIMM TEMP HACK - REMOVE LOCAL FOR ID RTT
    if ($runTag == "LOCAL" && $directory == "ID-RTT") continue;
    // TIMM TEMP HACK - REMOVE LOCAL FOR ID RTT
    $runNum = filter_var($explode[2], FILTER_SANITIZE_NUMBER_INT);
    $runDetails = array( $runTag, $runNum );
    array_push( $runTagAndNum, $runDetails );
  }
  return $runTagAndNum;
}

function getTrigConfigs( $dir, $type, $tag, $run ) { 
  $configs = array();
  $jsonFiles = glob( getConfig($dir, $type, $tag, $run) );
  foreach( $jsonFiles as &$jsonFile ) {
    $start = strrpos($jsonFile, "SM"); 
    $config = substr($jsonFile, $start, -5); //-4 to avoid .json
    array_push( $configs, $config );
  }
  sort( $configs );
  return array_unique( $configs ); 
}

function getRunRanges( $dir, $type, $tag, $run ) {
  global $runRanges; //Array of the different range strings to expect
  $ranges = array();
  $csvFiles = glob( getCsv($dir, $type, $tag, $run) );
  //print_r($csvFiles);
  foreach( $csvFiles as &$csvFile ) { // Loop over files
    foreach( $runRanges as $checkRunRange ) { // Loop over possible ranges
      if ( strpos($csvFile, $checkRunRange) !== FALSE) {
        $start = strrpos($csvFile, $checkRunRange);
        $range = substr($csvFile, $start, -4); //-4 to avoid .csv
        array_push( $ranges, $range );
        break;
      }      
    }
  }
  sort( $ranges );
  return array_unique( $ranges ); 
}
  
function getFullEventSummary($dir, $type, $tag, $run, $level ) {
  $events = array();
  $csvFiles = glob( getCsv($dir, $type, $tag, $run, "*", $level, "Full_Evnt") ); // Summary always Full_Evnt
  //print_r($csvFiles);
  foreach( $csvFiles as &$csvFile ) {
    $start = strrpos($csvFile, "Event"); // Range always starts with Event_
    $event = substr($csvFile, $start, -4); //-4 to avoid .csv
    array_push( $events, $event );
  }
  sort( $events );
  return array_unique( $events ); 
}

function getSlowEventSummary($dir, $type, $tag, $run, $level ) {
  $events = array();
  $csvFiles = glob( getCsv($dir, $type, $tag, $run, "*", $level, "Slow_Evnt") ); // Summary always Full_Evnts
  foreach( $csvFiles as &$csvFile ) {
    $start = strrpos($csvFile, "Event"); // Range always starts with Event_
    $event = substr($csvFile, $start, -4); //-4 to avoid .csv
    array_push( $events, $event );
  }
  sort( $events );
  return array_unique( $events ); 
}

function getRangeSummary($dir, $type, $tag, $run, $range, $level, $summaryItems ) { 
  $summaries = array();
  $csvFiles = glob( getCsv($dir, $type, $tag, $run, $range, $level) );
  //print_r($csvFiles);
  foreach( $summaryItems as $checkRunSummary ) { // Loop over possible ranges
    foreach( $csvFiles as &$csvFile ) {
      //echo "|check " . $checkRunSummary . " in " . $csvFile ;
      if (strpos($csvFile, "Table_" . $checkRunSummary . "_") !== FALSE) {
        array_push( $summaries, $checkRunSummary );
        break;
      }
    }
  }
  sort( $summaries );
  return array_unique( $summaries ); 
}

function getRatesLumiPoint($dir, $type, $tag, $run, &$lumiFloat, &$lumiStr, &$cmtStr ) {
  if (checkMetadata($dir, $type, $tag, $run) === 0) return;
  $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
  $md = $runMetadata["children"];
  if (isset($md)) {
    $predLumiCLI = 0;
    $predLumiMenuXML = 0;
    $predLumiRunXML = 0;
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if ($key == "PredictionLumiMenuXML") $predLumiMenuXML = $value;
        else if ($key == "PredictionLumiRunXML") $predLumiRunXML = $value;
        else if ($key == "PredictionLumi") $predLumiCLI = $value;
        else if ($key == "CMTPATH") $cmtStr = getVersionFromCMTPATH($value);
      }
    }
    if ($predLumiCLI > 0) {
      $lumiStr = "user specified";
      $lumiFloat = $predLumiCLI;
    } else if ($predLumiMenuXML > 0) {
      $lumiStr = "prescale XML specified";
      $lumiFloat = $predLumiMenuXML;
    } else if ($predLumiRunXML > 0) {
      $lumiStr = "default";
      $lumiFloat = $predLumiRunXML;
    }
  }
}

function getSingleMetadata($dir, $type, $tag, $run, $keyToGet) {
  if (checkMetadata($dir, $type, $tag, $run) === 0) return NULL;
  $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
  $md = $runMetadata["children"];
  if (isset($md)) {
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if ($key == $keyToGet) return $value;
      }
    }
  }
  return NULL;
}

function getLumiBlocksFromMeta($dir, $type, $tag, $run, $theConfig) {
  if (checkMetadata($dir, $type, $tag, $run) === 0) return NULL;
  $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
  $md = $runMetadata["children"];
  if (isset($md)) {
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if (strpos($key, 'LumiBlocksPerKeyset') !== false and strpos($value, $theConfig) !== false) {
          $result = explode(":",$value);
          return $result[1];
        }
      }
    }
  }
  return "";
}

// Some $dir directories contain sub-directories which we want to list. Here we work out what those sub directories are
function populateSubdirectoryArray($theDir, $subDirPath) {
  global $dirPostPatters;
  $subDir = array_filter(glob( $subDirPath . "/*"), 'is_dir');
  usort($subDir, create_function('$a,$b', 'return filemtime($a) - filemtime($b);'));
  $subDir = array_reverse($subDir, true);
  foreach ($subDir as &$e) {
    $e = substr($e, strrpos($e, '/') + 1);
    $ar = &$dirPostPatters[$theDir];
    $ar[$e] = "/" . $e;
  }
}

$rateSubDir = array_reverse( array_filter(glob("/data/RateProcessings_2016/*"), 'is_dir') );
foreach ($rateSubDir as &$e) {
  $e = substr($e, strrpos($e, '/') + 1);
  $ar = &$dirPostPatters["RateProcessings-2016"];
  $ar[$e] = "/" . $e;
}

function getLinkPage($page) {
  return "?page={$page}";
}

function getLinkDir($dir) {
  return "?dir={$dir}";
}

function getLinkRun($dir, $type, $tag, $run) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}";
}

function getLinkRange($dir, $type, $tag, $run, $range) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;range={$range}";
}

function getLinkConfig($dir, $type, $tag, $run, $config) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;config={$config}";
}

function getLinkMetadata($dir, $type, $tag, $run) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;page=metadata";
}

function getLinkConfigSearch($dir, $type, $tag, $run, $config, $search) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;config={$config}&amp;search={$search}";
}

function getLinkSummary($dir, $type, $tag, $run, $range, $level, $summary) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;range={$range}&amp;level={$level}&amp;summary={$summary}";
}

function getLinkItem($dir, $type, $tag, $run, $range, $level, $summary, $item) {
  return "?dir={$dir}&amp;type={$type}&amp;tag={$tag}&amp;run={$run}&amp;range={$range}&amp;level={$level}&amp;summary={$summary}&amp;item={$item}";
}

//

function getCsv($dir, $type, $tag, $run, $range = "*", $level = "*", $summary = "*") {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/csv/Table_{$summary}_{$level}_{$range}.csv";
}

function getConfig($dir, $type, $tag, $run, $config = "*") {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/TriggerConfiguration_{$config}.json";
}

function getXML($dir, $type, $tag, $run, $range, $level, $summary) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/xml/TrigRate_{$tag}_{$level}_{$range}.xml";
}

function getJSON($dir, $type, $tag, $run, $range, $level, $summary) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/rates.json";
}

function getRoot($dir, $type, $tag, $run) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/TrigCostRoot_Results.root";
}

function getMetadata($dir, $type, $tag, $run) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/metadata.json";
}

function getInProgress($dir, $type, $tag, $run) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/progress.json";
}

function getRatesGraph($dir, $type, $tag, $run) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}/ratesGraph.json";
}

function getRun($dir, $type, $tag, $run) {
  $subDir = getSubstitutedUrl($dir, $type, $tag, $run);
  return "{$dir}{$subDir}/costMonitoring_{$tag}_{$run}";
}

function checkRun($dir, $type = "*", $tag = "*", $run = "*") {
  $dirs = glob( getRun($dir, $type, $tag, $run), GLOB_ONLYDIR);
  return count( $dirs );
}

function checkCsv($dir, $type, $tag, $run, $range = '*', $level = '*', $summary = '*') {
  $csvFiles = glob( getCsv($dir, $type, $tag, $run, $range, $level, $summary) );
  return count( $csvFiles );
}

function checkRoot($dir, $type, $tag, $run) {
  $rootFiles = glob( getRoot($dir, $type, $tag, $run) );
  return count( $rootFiles );
}

function checkXML($dir, $type, $tag, $run, $range, $level, $summary) {
  $xmlFile = glob( getXML($dir, $type, $tag, $run, $range, $level, $summary) );
  return count( $xmlFile );
}

function checkJSON($dir, $type, $tag, $run, $range, $level, $summary) {
  $xmlFile = glob( getJSON($dir, $type, $tag, $run, $range, $level, $summary) );
  return count( $xmlFile );
}

function checkConfig($dir, $type, $tag, $run, $config = "*") {
  $configFiles = glob( getConfig($dir, $type, $tag, $run, $config) );
  return count( $configFiles );
}

function checkMetadata($dir, $type, $tag, $run) {
  $mdFiles = glob( getMetadata($dir, $type, $tag, $run) );
  return count( $mdFiles );
}

function checkInProgress($dir, $type, $tag, $run) {
  $inProg = glob( getInProgress($dir, $type, $tag, $run) );
  return count( $inProg );
}

function checkRatesGraph($dir, $type, $tag, $run) {
  $rgFiles = glob( getRatesGraph($dir, $type, $tag, $run) );
  return count( $rgFiles );
}

  
?>

