<?php 

function page_processingRequest() {
  global $err, $isAdmin;

  $filesRoot = "/var/www/TrigCostWeb/filelists/";
  $commandsRoot = "/var/www/TrigCostWeb/commands/";
  $costRoot = "/data/CostProcessings_LS2/";
  $ratesRoot = "/data/RateProcessings_LS2/";
  $timestamp = time();

  // First submit data
  $rateFiles = isset($_POST['rateFiles']) ? sanitise($_POST['rateFiles']) : NULL;
  $costFiles = isset($_POST['costFiles']) ? sanitise($_POST['costFiles']) : NULL;
  $nFiles = 0;
  $doRates = isset($_POST['doRates']) ? TRUE : FALSE;
  $doCost = isset($_POST['doCost']) ? TRUE : FALSE;
  $doValidate = isset($_POST['doValidate']) ? TRUE : FALSE;
  $skipValidate = (isset($_POST['skipValidate']) and $isAdmin == TRUE) ? TRUE : FALSE;
  $prescaleTag = isset($_POST['prescaleTag']) ? sanitise($_POST['prescaleTag']) : 'noPS';
  $JIRA = isset($_POST['JIRA']) ? sanitise($_POST['JIRA']) : NULL;
  $JIRAFull = NULL;
  $release = isset($_POST['release']) ? sanitise($_POST['release']) : NULL;
  $runNumber = isset($_POST['runNumber']) ? intval(sanitise($_POST['runNumber'])) : NULL;
  $description = isset($_POST['description']) ? sanitise($_POST['description']) : NULL;

  // Second submit data
  $commandRates = isset($_POST['commandRates']) ? sanitise($_POST['commandRates']) : NULL;
  $commandCost = isset($_POST['commandCost']) ? sanitise($_POST['commandCost']) : NULL;
  $rateFilesSubmit = isset($_POST['rateFilesSubmit']) ? sanitise($_POST['rateFilesSubmit']) : NULL;
  $costFilesSubmit = isset($_POST['costFilesSubmit']) ? sanitise($_POST['costFilesSubmit']) : NULL;
  $costInputFile = isset($_POST['costInputFile']) ? sanitise($_POST['costInputFile']) : NULL;
  $rateInputFile = isset($_POST['rateInputFile']) ? sanitise($_POST['rateInputFile']) : NULL;
  $costDirName = isset($_POST['costDirName']) ? sanitise($_POST['costDirName']) : NULL;
  $rateDirName = isset($_POST['rateDirName']) ? sanitise($_POST['rateDirName']) : NULL;
  $ratesCommandFile = isset($_POST['ratesCommandFile']) ? sanitise($_POST['ratesCommandFile']) : NULL;
  $costCommandFile = isset($_POST['costCommandFile']) ? sanitise($_POST['costCommandFile']) : NULL;
  $release = isset($_POST['release']) ? sanitise($_POST['release']) : NULL;
  $doSubmit = isset($_POST['doSubmit']) ? TRUE : FALSE;

  $status = "unvalidated";

  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  if ($skipValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
    $status = "validated"; // Yeah, it's fine I'm sure
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ($doValidate == TRUE) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // CHECK INPUT FILES
    if ( ($costFiles == NULL or $costFiles == "") and ($rateFiles == NULL or $rateFiles == "") ) {
      $err[] = "ERROR: No files supplied";
    }
    $fileArray = explode ("\n", $costFiles);
    $nFiles = count($fileArray);
    $hasCostFiles = FALSE;
    foreach ($fileArray as $line) {
      if ($line == "") continue;

      if (substr($line,0,5) != "/afs/" and substr($line,0,5) != "/eos/" and substr($line,0,7) != "/cvmfs/" and substr($line,0,21) != "root://eosatlas//eos/") {
        $err[] = "ERROR: Invalid input file '{$line}'";
        break;
      };
      // Work out the file type
      if (strpos($line, "NTUP_TRIGCOST") !== FALSE or strpos($line, "trig_cost") !== FALSE) {
        $hasCostFiles = TRUE;
        $costFilesSubmit[] = $line;
      } else {
        $err[] = "ERROR: Cost input file name must contain either 'NTUP_TRIGCOST' or 'trig_cost'.";
        break;
      }
    }

    $fileArray = explode ("\n", $rateFiles);
    $nFiles += count($fileArray);
    $hasRateFiles = FALSE;
    foreach ($fileArray as $line) {
      if ($line == "") continue;

      if (substr($line,0,5) != "/afs/" and substr($line,0,5) != "/eos/" and substr($line,0,7) != "/cvmfs/" and substr($line,0,21) != "root://eosatlas//eos/") {
        $err[] = "ERROR: Invalid input file '{$line}'";
        break;
      };

      if (strpos($line, "NTUP_TRIGRATE") !== FALSE) {
        $hasRateFiles = TRUE;
        $rateFilesSubmit[] = $line;
      } else {
        $err[] = "ERROR: Rate input file name must contain either NTUP_TRIGRATE' or 'trig_rate'.";
        break;
      }
    }

    // CHECK TYPE
    if ($doRates == FALSE and $doCost == FALSE) $err[] = "ERROR: Must choose at least one of Rates or Cost mode";
    if ($doCost == TRUE and $hasCostFiles == FALSE) $err[] = "ERROR: Cannot do a cost processing without 'NTUP_TRIGCOST' input file(s).";
    if ($doRates == TRUE and $hasRateFiles == FALSE) $err[] = "ERROR: Cannot do a rates processing without 'NTUP_TRIGRATE' input file(s).";

    // CHECK JIRA
    if ($JIRA != NULL) {
      if (is_numeric($JIRA) == false) $err[] = "ERROR: Invalid JIRA given. Must be a number.";
      else $JIRAFull = "ATR-" . $JIRA;
    } 

    // CHECK RELEASE
    if ($release == NULL) {
      $err[] = "ERROR: Release number must be supplied.";
    } else {
      $releaseExplode = explode (".", $release);
      $releaseStyle = true;
      foreach ($releaseExplode as $element) {
        if (is_numeric($element) === FALSE) {
          $releaseStyle = false;
          break;
        }
      }

      if ($releaseStyle === FALSE && DateTime::createFromFormat('?Y#m#d', $release) === FALSE) { // Test time style
        $err[] = "ERROR: Invalid release number given. Should numbers separated by full-stops or of the format rYYYY-MM-DD";
      }
    }

    // CHECK DESCRIPTION
    if ($description != NULL) {
      if (strpos($description, " -") !== FALSE) $err[] = "ERROR: Description may not contain the pattern ' -'.";
    }

    if(!empty($err)) echoErrorMsgs();
    else $status = "validated";
    
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  } else if ( $doSubmit == TRUE ) {
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

    // MAKE A LOOSER CHECK OF THE RESUBMITTED DATA
    if ($costFilesSubmit == NULL and $rateFilesSubmit == NULL) $err[] = "ERROR: Did not receive files.";
    if ($doCost == TRUE and $costInputFile == NULL) $err[] = "ERROR: Cost file path was not transmitted.";
    if ($doRates == TRUE and $rateInputFile == NULL) $err[] = "ERROR: Rate file path was not transmitted.";
    if ($commandRates == NULL and $commandCost == NULL) $err[] = "ERROR: Did not receive command to execute.";
    if ($commandRates != NULL and $ratesCommandFile == NULL) $err[] = "ERROR: Did not receive path to save rates command to.";
    if ($commandCost  != NULL and $costCommandFile == NULL) $err[] = "ERROR: Did not receive path to save cost command to.";
    if(!empty($err) and $isAdmin == FALSE) { // Admins get to skip this validation too as they can fix anything which goes wrong
      $err[] = "ERROR: Some data was not received. Please go back one page and make sure no text boxes are left empty";
      $status = "err"; // This will mean we don't show anything else below
    } else {
      $status = "submit";
    }
    echoErrorMsgs();


  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
  }
  //\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

  // IF VALIDATED - SHOW PROTO-COMMAND
  /////////////////////////////////////////////////////////////////////////////////////////////
  if ($status == "validated") {
  /////////////////////////////////////////////////////////////////////////////////////////////
    
    // OUTPUT FILE INFO
    echo "<form action='?page=ProcessingRequest' method='post'>";

    echo "<h2>Input Files Summary</h2>";
    echo "<div class='subtitle'>{$nFiles} files are to be processed.</span></div>";
    echo "<h3>Rate Input Files Summary</h3>";
    echo "<p><textarea name='rateFilesSubmit' rows='10' cols='150'>{$rateFiles}</textarea></p>";
    echo "<h3>Cost Input Files Summary</h3>";
    echo "<p><textarea name='costFilesSubmit' rows='10' cols='150'>{$costFiles}</textarea></p>";

    // User details
    $userDetails = "";
    if ($description != NULL) $userDetails = "--userDetails '{$description}'";   

    // Prepare cost/rate command
    if ($doCost == TRUE) {
      echo "<h2>Cost Processing Command Summary</h2>";

      // Work out where the command will be written
      $costCommandFile = "{$commandsRoot}cost_{$JIRAFull}_{$timestamp}.sh";
      echo "<input type='hidden' name='costCommandFile' value='{$costCommandFile}' />";

      # Create cost directory path
      $costDir = "costMonitoring_cost-reprocessing";
      if ($JIRAFull != NULL) $costDir .= "-" . $JIRAFull;
      $costDir .= "_" . $runNumber;

      $costInputFile = "{$costRoot}{$release}/{$costDir}/TrigCostRoot_Results.root";

      echo "<input type='hidden' name='costInputFile' value='{$costInputFile}' />";
      echo "<input type='hidden' name='costDirName' value='{$costDir}' />";

      // Construct Command
      $commandCost = "CostAnalysisPostProcessing.py --file {$costInputFile} {$userDetails}";

      echo "<p><textarea name='commandCost' rows='3' cols='150'>{$commandCost}</textarea></p>";
    }
    if ($doRates == TRUE) {
      echo "<h2>Rates Processing Command Summary</h2>";

      // Work out where the command will be written
      $ratesCommandFile = "{$commandsRoot}rate_{$JIRAFull}_{$timestamp}.sh";
      echo "<input type='hidden' name='ratesCommandFile' value='{$ratesCommandFile}' />";

      // Work out an output tag name
      $rateTag = "rate-prediction-";
      if ($JIRAFull != NULL) $rateTag .= $JIRAFull . "-";
      $formattedPrescaleTag =  str_replace(array("/", " ", "_"), "-", $prescaleTag); // Switch _ to - to perserve formatting
      $rateTag .= $formattedPrescaleTag;

      # Create cost directory path
      $rateDir = "costMonitoring_{$rateTag}_{$runNumber}";

      $rateInputFile = "{$ratesRoot}{$release}/{$rateDir}/TrigCostRoot_Results.root";
      echo "<input type='hidden' name='rateInputFile' value='{$rateInputFile}' />";
      echo "<input type='hidden' name='rateDirName' value='{$rateDir}' />";



      // Construct Command
      $commandRates =  "RatesAnalysisPostProcessing.py --file {$rateInputFile} --outputTag {$rateTag} {$userDetails}";
      echo "<p><textarea name='commandRates' rows='3' cols='150'>{$commandRates}</textarea></p>";
    }

    // pass release to save it to file
    echo "<input type='hidden' name='release' value='{$release}' maxlength='15' />";

    // final submit button
    echo "<h2>Submit</h2>";
    echo "<div class='subtitle'>Commands have been generated from your input, please check them and make any alterations which may be required before submitting the request.</div>";
    echo "<input type='submit' value='Queue Processing' name='doSubmit'/>";
    echo "</form>";

  /////////////////////////////////////////////////////////////////////////////////////////////    
  } else if ($status == "submit") {
  /////////////////////////////////////////////////////////////////////////////////////////////

    // DO THE SUBMISSION AND SHOW THE SUMMARY
    echo "<form action='?page=ProcessingRequest' method='post'>"; // We never actually submit this

    $commandPresetup = "export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase \n";
    $commandPresetup .= "source \${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh \n";
    $commandPresetup .= "source \${AtlasSetup}/scripts/asetup.sh master,r25,Athena \n";

    // Save cost/rate command to file
    if ($commandCost != NULL and $costCommandFile != NULL) {
      echo "<h2>Cost Processing</h2>";

      // Create directories
      $command = $commandPresetup;
      $command .= "mkdir -p {$costRoot}{$release}/{$costDirName}/csv \n";

      // Create command to merge input files
      if ($costFilesSubmit != NULL and $costInputFile != NULL) {
        echo "<h2>Input Files</h2>";

        $costFilesSubmit = str_replace("\r", " ", $costFilesSubmit); // We need unix line breaks!
        $costFilesSubmit = str_replace("\n", " ", $costFilesSubmit);

        $nFiles = str_word_count($costFilesSubmit, 0, "._-/1234567890"); // File name can contain that symbols
        echo "<script> console.log({$nFiles})</script> ";
        if ($nFiles == 1) {
          $commandMerge = "cp {$costFilesSubmit} {$costInputFile}";
        } else {
          $commandMerge = "hadd {$costInputFile} {$costFilesSubmit}";
        }

        echo "<p>File list will be merged to files: '<span class='codePara'>{$costInputFile}</span>'</p>";
        echo "<p><textarea disabled name='costFilesSubmit' rows='10' cols='150'>{$costFilesSubmit}</textarea></p>";
      }

      $command .= "{$commandMerge} \n{$commandCost} \n";
      // Move the result to the correct directory
      $command .= "mv Table* {$costRoot}{$release}/{$costDirName}/csv \n";
      if (file_put_contents($costCommandFile, $command) === FALSE) echo "<p><span class='processing'>Error writing '<span class='codePara'>{$costCommandFile}</span>'!</span></p>";
      else echo "<p>Command written to: '<span class='codePara'>{$costCommandFile}</span>'</p>";
      echo "<p><textarea disabled name='commandCost' rows='3' cols='150'>{$commandCost}</textarea></p>";
    }

    if ($commandRates != NULL and $ratesCommandFile != NULL) {
      echo "<h2>Rate Processing</h2>";

      // Create directories
      $command = $commandPresetup;
      $command .= "mkdir -p {$ratesRoot}{$release}/{$rateDirName}/csv \n";

      if ($rateFilesSubmit != NULL and $rateInputFile != NULL) {
        echo "<h2>Input Files</h2>";

        $rateFilesSubmit = str_replace("\r", " ", $rateFilesSubmit); // We need unix line breaks!
        $rateFilesSubmit = str_replace("\n", " ", $rateFilesSubmit);

        $nFiles = str_word_count($rateFilesSubmit, 0, "._-/1234567890"); // File name can contain that symbols
        echo "<script> console.log({$nFiles})</script> ";
        if ($nFiles == 1) {
          $commandMerge = "cp {$rateFilesSubmit} {$rateInputFile}";
        }
        else {
          $commandMerge = "hadd {$rateInputFile} {$rateFilesSubmit}";
        }

        echo "<p>File list will be merged to file: '<span class='codePara'>{$rateInputFile}</span>'</p>";
        echo "<p><textarea disabled name='costFilesSubmit' rows='10' cols='150'>{$rateFilesSubmit}</textarea></p>";
      }

      $command .= "{$commandMerge} \n{$commandRates} \n";
      // Move the result to the correct directory
      $command .= "mv Table* {$ratesRoot}{$release}/{$rateDirName}/csv \n";
      $command .= "mv -t {$ratesRoot}{$release}/{$rateDirName} rates.json metadata.json \n";
      if (file_put_contents($ratesCommandFile, $command) === FALSE) echo "<p><span class='processing'>Error writing '<span class='codePara'>{$ratesCommandFile}</span>'!</span></p>";
      else echo "<p>Command written to: '<span class='codePara'>{$ratesCommandFile}</span>'</p>";
      echo "<p><textarea disabled name='commandRates' rows='3' cols='150'>{$commandRates}</textarea></p>";
    }

    echo "<h2>Submission Finished</h2>";
    echo "<p>When not processing other manual requests, the <span class='codePara'>atlas-trig-cost</span> VM should notice this request within 60 seconds.</p>";
    echo "<p>To view the VMs processing status please visit the <a href='" . getLinkPage("logManual") . "'><strong>logs</strong></a> page.</p>";
    echo "<p>Any mistakes or problems? Contact <strong>atlas-trigger-rate-expert@cern.ch</strong>.</p>";
    echo "</form>"; // We never actually submit this
    // This file tells the trig cost to wake from sleep
    file_put_contents("/var/www/TrigCostWeb/breakSleepManual", "breakSleepManual");

  /////////////////////////////////////////////////////////////////////////////////////////////
  } else if ($status == "unvalidated") { // Show data collection window
  /////////////////////////////////////////////////////////////////////////////////////////////

    $rateChecked = (($doRates == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $costChecked = (($doCost  == TRUE or $_SERVER['REQUEST_METHOD'] != 'POST') ? "checked='checked'" : "");
    $adminSkipValidation = ($isAdmin == TRUE ? "<p><input type='submit' value='Skip Validation [ADMIN]' name='skipValidate'/></p>" : "");

echo <<< EXPORT
<p>This interface allows trigger experts to schedule the processing of full <span class='codePara'>NTUP_TRIGCOST</span> or slimmed <span class='codePara'>NTUP_TRIGRATE</span> ntuples on the <span class='codePara'>atlas-trig-cost</span> virtual machine.</p>

<form action='?page=ProcessingRequest' method='post'>
  
  <h2>Input Files</h2>
  <div class='subtitle'>One file per line.</div>
  <div class='subtitle'>Must be accessible at CERN, allowed paths start with:  <span class='codePara'>/afs/, /cvmfs/, /eos/, root://eosatlas//eos/</span></div>
  <div class='subtitle'>Suggest grid command to find files at CERN <strong>(COST files are sometimes on CERN-PROD_DATADISK)</strong>: <span class='codePara'>rucio list-file-replicas --rse CERN-PROD_TRIG-HLT <i>DATASET_NAME_HERE</i> | grep -oE /eos/.*\.root\.1</span></div>
  <h3>Rate Input Files</h3>
  <p><textarea name='rateFiles' rows='10' cols='150'>{$rateFiles}</textarea></p>
  <h3>Cost Input Files</h3>
  <p><textarea name='costFiles' rows='10' cols='150'>{$costFiles}</textarea></p>

  <h2>Processing Type</h2>
  <div class='subtitle'>Cost processing (CPU time) requires <span class='codePara'>COST</span> ntuples, rate processing can use either <span class='codePara'>RATE</span> or <span class='codePara'>COST</span> ntuples.</div>
  <div class='subtitle'>Cost processing may be done for any run. Rates can only be done for reprocessed EnhancedBias data.</div>
  <p><input type='checkbox' name='doRates' value='true' {$rateChecked} />Rates Processing (from NTUP_TRIGRATE files)</p>
  <p><input type='checkbox' name='doCost' value='true' {$costChecked} />Cost Processing (from NTUP_TRIGCOST files)</p> 

  <h2>Prescale Tag (Optional)</h2>
  <div class='subtitle'>Provide tag to differ reprocessings with diffrent prascales sets.</div>
  <p>Prescale Tag: <input type='text' name='prescaleTag' value='{$prescaleTag}' maxlength='200'/></p>

  <h2>JIRA Link (Optional)</h2>
  <div class='subtitle'>Enter any trigger JIRA bug/task number for easier reference.</div>
  <p>ATR-<input type='text' name='JIRA' value='{$JIRA}' maxlength='10' /></p>

  <h2>Run Number</h2>
  <p>Run Number:<input type='text' name='runNumber' value='{$runNumber}' maxlength='15' /></p>

  <h2>Release Number</h2>
  <div class='subtitle'>Enter the release or nightly used to run the trigger e.g. <span class='codePara'>21.1.1</span> or <span class='codePara'>r2017-04-03</span>.</div>
  <p>Release: <input type='text' name='release' value='{$release}' maxlength='15' /></p>

  <h2>Description</h2>
  <div class='subtitle'>Please briefly describe the request. What was aim of the reprocessing? Were rereunL1 or applyL1 modifiers used? Was the TriggerMenu tag non-standard? Which menu was used?</div>
  <p>Description: <input type='text' name='description' value='{$description}' maxlength='400'/></p>

  <h2>Check Submission</h2>
  <p><input type='submit' value='Validate' name='doValidate'/></p>
  {$adminSkipValidation}

</form>

EXPORT;
  /////////////////////////////////////////////////////////////////////////////////////////////
  }
  /////////////////////////////////////////////////////////////////////////////////////////////

}


?>
