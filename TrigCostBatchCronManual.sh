#!/bin/bash
exec >> /data/cronManual.log 2>&1

#setup eos
#export PATH=$PATH:/afs/cern.ch/project/eos/installation/pro/bin/
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/afs/cern.ch/project/eos/installation/pro/lib64/
export EOS_MGM_URL=root://eosatlas.cern.ch

# This bash script looks in predetermined locations for new input data and runs TrigCost over it.
# It should be run by a CRON job every ~30m
echo "###### Executing TrigCostBatchCronManual on `hostname` - `date`"

################################################## ##################################################

# Config manual locations
MANUAL_LOCATION[0]="/var/www/TrigCostWeb/commands/"
#MANUAL_LOCATION[1]="/afs/cern.ch/work/t/tamartin/public/CostProcessing/"
#MANUAL_LOCATION[2]="/afs/cern.ch/user/a/amyers/public/CostProcessing/"

# Config file of already processed locations
ALREADY_PROCESSED_FILELIST=/opt/trig-cost-sw/processed.txt

################################################## ##################################################

# SW update only in TrigCostBatchCron.sh

# check for manual command
if [ -z "$TO_PROCESS" ]; then 
  for LOCATION in "${MANUAL_LOCATION[@]}"; do # Look at each search location
    for MANCOM in `find $LOCATION -not -path '*/\.*' -type f`; do # the -not -path bit ignores hidden (.gitignore) file
      HAVE_PROCESSED_MANUAL=false
      while read PROCESSED; do # Check each of these against the list of processed directories
        if [ "$MANCOM" == "$PROCESSED" ]; then
          HAVE_PROCESSED_MANUAL=true
          break
        fi
      done < $ALREADY_PROCESSED_FILELIST
      if [ "$HAVE_PROCESSED_MANUAL" = true ]; then
        continue
      fi
      # Do manual processing
      echo $MANCOM >> $ALREADY_PROCESSED_FILELIST
      /var/www/TrigCostWeb/TrigCostBatchExecuteManual.sh $MANCOM
      echo "Manual Processing done. Exit."
      exit
    done
  done
fi

echo "No new directories to process. Exit."
