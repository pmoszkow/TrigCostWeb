<?php
class CostHistograms {

  var $ROOTFile;
  var $directories = array();
  var $directoryStr;
  
  function __construct($ROOTFile) {
    $this->ROOTFile = $ROOTFile;
  }
  
  function addDirectory($dir) {
    $this->directories[] = $dir;
    $this->directoryStr .= $dir . "/";
  }

  function insertHistograms() {
echo <<< EXPORT
<p>
Histograms from ROOT file: <a href="{$this->ROOTFile}">{$this->ROOTFile}</a>,<br/>TDirectory: {$this->directoryStr}
</p>
<div id='msgRep'></div>
<div class='column'>
<input type='hidden' name='state' value='{$this->ROOTFile}' id='urlToLoad'/>
<div id='reportA'>
</div></div><div class='column'><div id='reportB'></div></div>

<script type="text/javascript">
// <![CDATA[

var nav = [];
var readLevel = 0; // Current TDirectory search level
var readHist = -1; // Current histogram (-1 if still navigating directories)

EXPORT;
      foreach ($this->directories as &$directory) {
        echo "nav.push('{$directory}');\n";
      }
echo <<< EXPORT

window.onload = function () {
  JSROOT.openFile("{$this->ROOTFile}")
    .then( file => {
      console.log("ROOT File open!");
    })
    .catch( e => console.log("Error while opening the file: " + e.message));
}

// Use end of initial file read to trigger directory reading
function userCallback(file) {
  userReadKeyCallback(file); // Begin the directory search loop
}

// Use end of ReadKey to trigger reading next directory
function userReadKeyCallback(file) {
  if (readLevel > nav.length - 1) { // If at bottom TDirectory
    if (readHist == -1) {
      readHist = 0; // Begin the histogram output loop
      userObjBufferCallback(file);
    }
    return;
  }
  var keys = [];
  if (readLevel == 0) {
    keys = file.fKeys; // First call, root TDirectories from file
  } else {
    keys = file.fDirectories[readLevel-1].fKeys; // Latter call, TDirectories from current directory
  }
  var found = false; 

  keys.reverse(); // why?
  for (var i = 0; i < keys.length; ++i) {
    keyName = keys[i].fName;
    if ( keyName == nav[readLevel] ) { // Check if key name contains search string
      var fullName = nav[0];
      for(var j = 1; j <= readLevel; ++j)
        fullName += '/' + nav[j];

      file.readDirectory(fullName, keys[i].fCycle, 0); // Read the directory
        found = true;
        break;
    }
  }
  if (found == false) console.log("TDirectory Not Found.");
  readLevel += 1;
}

// Use end of ObjectBuffer to trigger reading next histogram
function userObjBufferCallback(file) {
  console.debug("Reading histogram " + readHist);
  if (readHist == -1) return; // If not yet at destination TDirectory
  var dirDepth = nav.length - 1; // Depth of final TDirectory
  var nHists = file.fDirectories[dirDepth].fKeys.length - 1; // N things to plot
  if (nHists == -1) {
    console.log("Warning! No histograms (" + nHists + ") found in directory");
  }
  if (readHist > nHists) return;
  if (file.fDirectories[dirDepth].fKeys[readHist].fName.indexOf("type") > -1) return; // This is a temp hack
  histName = file.fDirectories[dirDepth].fKeys[readHist].fName;
  console.debug("Drawing " + histName);
  addElement(histName, readHist);
  file.fDirectories[dirDepth].readObject(histName)
    .then(obj => {
      divName = obj.fName;
      options = "";
      if (obj.fXaxis.fXmax > 9999)
        options += "LOGX ";
      if (obj.fYaxis.fYmax > 9999)
        options += "LOGY ";
      if (obj._typename.includes("TH2"))
        options += "COLZ ";
        
      JSROOT.draw(divName, obj , options)
    });
    readHist += 1;
}

// Create div element for histogram
function addElement (histName, histNumber) {
  var histDiv = document.createElement("DIV");
  histDiv.setAttribute("id", histName);
  histDiv.setAttribute("class", "histogram");
  divParent = histNumber%2 ? "reportB" : "reportA";
  document.getElementById(divParent).appendChild(histDiv);
}

// ]]>
</script>
EXPORT;
  }
  
}
?>
