<?php 

function page_compareRates() {

  // Read any POST data
  $compA = isset($_POST['compA']) ? sanitise($_POST['compA']) : NULL;
  $compB = isset($_POST['compB']) ? sanitise($_POST['compB']) : NULL;
  if (strpos($compB, "Paste the") !== FALSE) $compB = NULL;
  $lumi = isset($_POST['lumi']) ? sanitise($_POST['lumi']) : NULL;

  $costTable = new CostDataTable();
  $costTable->setTitle("CompareRates");

  $doParse = isset($compA) and $compA != "" and isset($compB) and $compB != "";

  // Decode
  $lumiA = 0;
  $lumiB = 0;
  if ($doParse == TRUE) {
    $arrayA = array();
    parse_str(parse_url(htmlspecialchars_decode($compA), PHP_URL_QUERY), $arrayA);
    $dir = isset($arrayA["dir"]) ? $arrayA["dir"] : NULL;
    $type = isset($arrayA["type"])  ? $arrayA["type"] : NULL;
    $tag = isset($arrayA["tag"]) ? $arrayA["tag"] : NULL;
    $run = isset($arrayA["run"]) ? $arrayA["run"] : NULL;
    $range = isset($arrayA["range"]) ? $arrayA["range"] : NULL;
    $level = isset($arrayA["level"]) ? $arrayA["level"] : NULL;
    $summary = isset($arrayA["summary"]) ? $arrayA["summary"] : NULL;

    $arrayB = array();
    parse_str(parse_url(htmlspecialchars_decode($compB), PHP_URL_QUERY), $arrayB);
    $dirB = isset($arrayB["dir"]) ? $arrayB["dir"] : NULL;
    $typeB = isset($arrayB["type"])  ? $arrayB["type"] : NULL;
    $tagB = isset($arrayB["tag"]) ? $arrayB["tag"] : NULL;
    $runB = isset($arrayB["run"]) ? $arrayB["run"] : NULL;
    $rangeB = isset($arrayB["range"]) ? $arrayB["range"] : NULL;
    $levelB = isset($arrayB["level"]) ? $arrayB["level"] : NULL;
    $summaryB = isset($arrayB["summary"]) ? $arrayB["summary"] : NULL;

    // Get the lumi of the two runs
    $cmtStrA = "unknown";
    $cmtStrB = "unknown";
    $dummy = ""; // Don't care how the L was selected
    getRatesLumiPoint($dir, $type, $tag, $run, $lumiA, $dummy, $cmtStrA );
    getRatesLumiPoint($dirB, $typeB, $tagB, $runB, $lumiB, $dummy, $cmtStrB );

  }

  if ($lumi == NULL) $lumi = ($lumiA != 0 ? $lumiA : "2e34");

  echo <<< EXPORT
  <p><strong>
  Paste the URL of two Rate Summary pages from the atlas-trig-cost site to perform a comparison.
  </strong></p>
  <form action="?page=CompareRates" method="post">
  <p>Comparison URL A: <input type="text" name="compA" value="{$compA}" /></p>
  <p>Comparison URL B: <input type="text" name="compB" value="{$compB}" /></p>
  <p>Luminosity Point: <input type="text" name="lumi" value="{$lumi}" /></p>
  <input type="submit" value="Do Comparison"/>
  </form>
EXPORT;

  if ($doParse == TRUE) {

    if (!is_numeric($lumi) or floatval($lumi) <= 0) {
      echo "<div class='err'>ERROR: Non-numeric or invalid luminosity given.</div>";
    } else if (!isset($dir) or !isset($type) or !isset($tag) or !isset($run) or !isset($range) or !isset($level) or !isset($summary)) {
      echo "<div class='err'>ERROR: Failed to parse cost data from URL A.</div>";
    } else if (!isset($dirB) or !isset($typeB) or !isset($tagB) or !isset($runB) or !isset($rangeB) or !isset($levelB) or !isset($summaryB)) {
      echo "<div class='err'>ERROR: Failed to parse cost data from URL B.</div>";
    } else if (checkCsv($dir, $type, $tag, $run, $range, $level, $summary) == 0) {
      echo "<div class='err'>ERROR: Cannot find data for URL A.</div>";
    } else if (checkCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB) == 0) {
      echo "<div class='err'>ERROR: Cannot find data for URL B.</div>";
    } else if (strpos($summary, "Rate_") === FALSE or strpos($summaryB, "Rate_") === FALSE) {
      echo "<div class='err'>ERROR: Both URLs must point to rates pages.</div>";
    } else if ($lumiA <= 0 or $lumiB <= 0) {
      echo "<div class='err'>ERROR: Was not able to determine the L point of the specified rates from the TrigCost processing metadata.</div>";
    } else {
      $csv  = getCsv($dir,  $type,  $tag,  $run,  $range,  $level,  $summary);
      $csvB = getCsv($dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB);

      // Allow quick switch between L1, HLT and group
      if ($summary == "Rate_ChainL1") {
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_ChainL1", "Rate_ChainHLT", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_ChainL1", "Rate_ChainHLT", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to HLT Rates'/></p></form>";
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_ChainL1", "Rate_Group", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_ChainL1", "Rate_Group", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to Group Rates'/></p></form>";
      } else if ($summary == "Rate_ChainHLT") {
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_ChainHLT", "Rate_ChainL1", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_ChainHLT", "Rate_ChainL1", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to L1 Rates'/></p></form>";
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_ChainHLT", "Rate_Group", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_ChainHLT", "Rate_Group", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to Group Rates'/></p></form>";
      } else if ($summary == "Rate_Group") {
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_Group", "Rate_ChainL1", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_Group", "Rate_ChainL1", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to L1 Rates'/></p></form>";
        echo "<form action='?page=CompareRates' method='post'>";
        echo "<input type='hidden' name='compA' value='" . str_replace("Rate_Group", "Rate_ChainHLT", $compA) . "' />";
        echo "<input type='hidden' name='compB' value='" . str_replace("Rate_Group", "Rate_ChainHLT", $compB) . "' />";
        echo "<p><input type='submit' value='Switch to HLT Rates'/></p></form>";
      }

      $lumi = floatval($lumi);
      if ($lumi < 1e10) $lumi *= 1e30;
      echo "<p>Rates comparison at <b>L = " . sprintf("%.2e",$lumi) . " cm<sup>-2</sup>s<sup>-1</sup></b></p>";
      echo "<p>Comparing Cache A: <b>${cmtStrA}</b> to Cache B: <b>${cmtStrB}</b></p>";

      $costTable->setDataSource( $csv,  $dir,  $type,  $tag,  $run,  $range,  $level,  $summary  );
      $costTable->setComparison( $csvB, $dirB, $typeB, $tagB, $runB, $rangeB, $levelB, $summaryB );
      $costTable->setCompLumi( $lumi, $lumiA, $lumiB );
      $costTable->addSearchCol(1); // Group
      $costTable->addSearchCol(2); // Matched
      $costTable->addSearchCol(3); // Matched ID
      $costTable->insertTable();


    }
  }
}

?>
