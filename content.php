<?php 

function content_homePage() {
  global $dir, $dataDirs2015, $dataDirs2016, $dataDirs2017, $dataDirs2018, $dataDirs2020, $USERNAME, $USER, $addLink;

  echoHeader(2,"Available Long Shutdown 2 Directories:");
  echo "<ul>\n";
  foreach ($dataDirs2020 as $theDirectory => $symLinkLocation) {
    echo "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a>\n";
    echo "  </li>";
  }
  echo "</ul>\n";

  echoHeader(2,"Available 2018 Directories:");
  echo "<ul>\n";
  foreach ($dataDirs2018 as $theDirectory => $symLinkLocation) {
    echo "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a>\n";
    echo "  </li>";
  }
  echo "</ul>\n";

  echoHeader(2,"Available 2017 Directories:");
  echo "<ul>\n";
  foreach ($dataDirs2017 as $theDirectory => $symLinkLocation) {
    echo "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a>\n";
    echo "  </li>";

  }
  echo "</ul>\n";

  echoHeader(2,"Available 2016 Directories:");
  echo "<ul>\n";
  foreach ($dataDirs2016 as $theDirectory => $symLinkLocation) {
    echo "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a>\n";
    echo "  </li>";

  }
  echo "</ul>\n";

  echoHeader(2,"Available 2015 Directories:");
  echo "<ul>\n";
  foreach ($dataDirs2015 as $theDirectory => $symLinkLocation) {
    echo "  <li><a href='" . getLinkDir($theDirectory) . "'>{$theDirectory}</a>\n";
    echo "  </li>";

  }
  echo "</ul>\n";

  echoHeader(2,"User's Cost Monitoring Data");
  if ( is_link($USERNAME) == true ) { // User has a simlink
    $linkLoc = readlink($USERNAME);
echo <<< EXPORT
<p>Hello <strong>{$USER}</strong>, each CERN user may set up a link to their own personal folder on <strong>/afs</strong> or <strong>/cvmfs</strong> 
which contains cost monitoring output files. Your directory soft link, <em>{$USERNAME}</em>, is setup and currently points to <strong>$linkLoc</strong>. 
If you would like to change this, you will first need to remove the current link.</p>
<p>
  <form action="." method="get">
  <input type="hidden" name="rmLink" value="REMOVE" class="largerInput" /><input type="submit" value="Remove My Data Link" />
  </form>
</p>   
EXPORT;
    $nRunsUser = checkRun($USERNAME);
    echo "  <ul><li><a href='" . getLinkDir($USERNAME) . "'>{$USERNAME}</a>\n";
    echo "  <div class='subtitle'>Number of Runs: {$nRunsUser}</div>";
    echo "  </li></ul>";
  } else { // User does not have a simlink
    if (isset($addLink)) {
      $boxText = $addLink; // If the user messed up the prev. link.
    } else {
      $boxText = "Enter public /afs or /cvmfs location";
    }
echo <<< EXPORT
<p>Hello <strong>{$USER}</strong>, each CERN user may set up a link to their own personal folder on <strong>/afs</strong> or <strong>/cvmfs</strong> 
which contains cost monitoring output files. By enterling a publicly readable location below, you will setup a soft link from this site to your folder - 
this will allow you <strong>and others</strong> to use this site's tools to browse the data you have processed. 
This directory should contain sub-directories in the <strong>"costMonitoring_%t_%r"</strong> format where <strong>%t</strong> is 
a processing tag and <strong>%r</strong> is a run number.</p>
<p>
  <form action="." method="get">
  <input type="text" name="addLink" value="{$boxText}" class="largerInput" /><input type="submit" value="Link In My Data" />
  </form>
</p>
EXPORT;
  }

  echoHeader(2, "Legacy Data");
  echo "<p>The majority of legacy run-1 data is located in the <a href='online'><strong>online</strong></a> directory in compressed archives. A small number of runs remain uncompressed.</p>\n"; 
  echo "<ul>\n";
  $legacyRuns = array('0212967' => '-BegLB108-EndLB123', '0213155' => '', '0214777' => '-BegLB267-EndLB282');
  foreach ($legacyRuns as $legacyRunNum => $legacyRunRange) {
    echo "  <li>Run Number:{$legacyRunNum} <a href='online/L2CostMonitoring/{$legacyRunNum}-L2CostMonitoring{$legacyRunRange}/'>L2CostMonitoring{$legacyRunRange}</a> <a href='online/EFCostMonitoring/{$legacyRunNum}-EFCostMonitoring{$legacyRunRange}/'>EFCostMonitoring{$legacyRunRange}</a>\n";
    echo "  <div class='subtitle'>Lookup in <a href='http://atlas-runquery.cern.ch/query.py?q=find+run+{$legacyRunNum}+/+show+all' class='linkExternal'>ATLAS Run Query</a></div>"; 
    echo "  </li>";
  }
  echo "</ul>\n";
}



function content_displayRuns() {
  global $dir, $dirPostPatters, $TT_RTTPath, $ID_RTTPath, $P1_RTTPath, $P1_RTTPath_dev, $TT_RTTPath_T0, $TT_RTTPath_mc16a,  $TT_RTTPath_dev;

  if ($dir == "TriggerTest-RTT") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath}</span>'</h3>";
  } else if ($dir == "ID-RTT") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$ID_RTTPath}</span>'</h3>";
  } else if ($dir == "TrigP1Test-RTT") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath}</span>'</h3>";
  } else if ($dir == "TrigP1Test-RTT-dev") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath_dev}</span>'</h3>";
  } else if ($dir == "TrigP1Test-RTT-mc16a") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$P1_RTTPath_mc16a}</span>'</h3>";
  } else if ($dir == "TriggerTest-RTT-T0") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath_T0}</span>'</h3>";
  } else if ($dir == "TriggerTest-RTT-dev") {
    echo "<h3>Displaying {$dir} data from '<span class='codePara'>{$TT_RTTPath_dev}</span>'</h3>";
  }

  // data sources
  if ( isset( $dirPostPatters[ $dir ] ) ) { // This directory has sub-directories

    //natsort($dirPostPatters[$dir]);
    //$reversed = array_reverse($dirPostPatters[$dir], true);
    foreach ( $dirPostPatters[$dir] as $theType => $theSubDir) {
      $availableData = getRuns($dir, $theType);
      echoHeader(3,"Runs in {$theType}:");
      echoTagRunDetails($availableData, $dir, $theType);
    }

  } else { // Simple case, no sub dirs
    $availableData = getRuns($dir);
    echoHeader(3,"Runs in {$dir}:");
    echoTagRunDetails($availableData, $dir);
  }
}



function content_displayRunRanges() {
  global $dir, $type, $tag, $run, $levels, $costData;  

  $availableData = getRunRanges($dir, $type, $tag, $run);

  $headTxt = "Run Ranges:";
  if (!(strpos($dir, 'data') === FALSE)) $headTxt = $headTxt . " <small>(Each SMK+L1PS+HLTPS monitors the first few contiguous LB which used that configuration)</small>";
  echoHeader(3,$headTxt); // Link to run ranges
  echo "<ul>\n";
  foreach($availableData as &$theRange) {
    echo "  <li><a href='" . getLinkRange($dir,$type,$tag,$run,$theRange) . "'>" . str_replace("_"," ",$theRange) . "</a></li>\n"; 
  }
  echo "</ul>\n";

  foreach ($levels as $theLevel) { // Link to single events (skip the summary selection screen)
    $fullEvents = getFullEventSummary( $dir, $type, $tag, $run, $theLevel );
    if ( count( $fullEvents) === 0 ) continue;
    echoHeader(3,"Single Random {$theLevel} Events:");
    echo "<ul>\n";
    foreach($fullEvents as &$theEvent) {
      echo "  <li><a href='" . getLinkSummary($dir,$type,$tag,$run,$theEvent,$theLevel,'Full_Evnt') . "'>" . str_replace("_"," ",$theEvent) ."</a></li>\n"; 
    }
    echo "</ul>\n"; 
  }

  foreach ($levels as $theLevel) { // Link to single slow events (skip the summary selection screen)
    $slowEvents = getSlowEventSummary( $dir, $type, $tag, $run, $theLevel );
    if ( count( $slowEvents) === 0 ) continue;
    echoHeader(3,"Single Slow {$theLevel} Events:");
    echo "<ul>\n";
    foreach($slowEvents as &$theEvent) {
      echo "  <li><a href='" . getLinkSummary($dir,$type,$tag,$run,$theEvent,$theLevel,'Slow_Evnt') . "'>" . str_replace("_"," ",$theEvent) ."</a></li>\n"; 
    }
    echo "</ul>\n"; 
  }
  
  echoHeader(3,"Trigger Configurations:"); // Link to trig configs
  $configs = getTrigConfigs($dir, $type, $tag, $run);
  echo "<ul>\n";
  foreach($configs as &$theConfig) {
    // Get array of the keys
    preg_match_all('!\d+!', $theConfig, $keyArray);
    $keys = $keyArray[0];
    echo "  <li><a href='" . getLinkConfig($dir,$type,$tag,$run,$theConfig) . "'>" . str_replace("_"," ",$theConfig) . "</a>\n";
    $lumiBlocks = getLumiBlocksFromMeta($dir, $type, $tag, $run, $theConfig);
    if ($lumiBlocks !== "") echo "  <div class='subtitle'>Lumi Blocks:" . $lumiBlocks . "<div>";
    echo "  <div class='subtitle'>Lookup in";
    // Note - the match_all gets the 1 from "L1" so we actually skip array entry #1
    echo " <a href='https://atlas-trigconf.cern.ch/run/smkey/{$keys[0]}/l1key/{$keys[2]}/hltkey/{$keys[3]}/' class='linkExternal'>ATLAS TrigConf</a></div>\n";
    echo "  </li>\n"; 
  }
  echo "</ul>\n";

  // Show root file link
  if (checkRoot($dir, $type, $tag, $run)) {
    echoHeader(3,"Histograms:");
    echo " <p>Processing's ROOT file: <a href='" . getRoot($dir, $type, $tag, $run) . "'>" . getRoot($dir, $type, $tag, $run) . "</a><p/>\n";
  }

  if (checkMetadata($dir, $type, $tag, $run) != false) {
    $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
    $md = $runMetadata["children"];
  }
  if (isset($md)) {
    echoHeader(3,"Run Processing Metadata:"); 
    echo "<p>\n";
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if ($key != "Date of Processing" && $key != "Version" && $key != "host" && $key != "pid"
          && $key != "AtlasProject" && $key != "CMTPATH" && $key != "SOURCE" && $key != "Command") continue;
        echo "<div class='subtitle'>{$key}: {$value}</div>\n"; 
      }
    }
    echo "</p>\n";
    echo "<p><a href='" . getLinkMetadata($dir, $type, $tag, $run) . "'>Show More Metadata</a></p>";
  }

}

function content_displayMetadata() {
  global $dir, $type, $tag, $run, $levels, $costData;  

  $runMetadata = json_decode ( file_get_contents(getMetadata($dir, $type, $tag, $run)), true );
  $md = $runMetadata["children"];

  if (isset($md)) {
    echoHeader(3,"Full Run Processing Metadata:"); 
    echo "<p>\n";
    foreach( $md as $mdEntry) {
      foreach( $mdEntry as $key => $value) {
        if( $key == "HLTMenu" ) continue;
        echo "<div class='subtitle'>{$key}: {$value}</div>\n"; 
      }
    }
    echo "</p>\n";
  }
}



function content_displayRangeSummaries() {
  global $levels, $dir, $type, $tag, $run, $range, $summaryList;  

  foreach ($levels as $theLevel) { 
    foreach($summaryList as $summaryTypeName => $summaryItems) {
      $availableData = getRangeSummary($dir, $type, $tag, $run, $range, $theLevel, $summaryItems);
      if ( count( $availableData) === 0 ) continue;
      echoHeader(3, "{$theLevel} {$summaryTypeName} Summaries:");
      echo "<ul>\n";
      foreach($availableData as &$theSummary) {
          echo "  <li><a href='" . getLinkSummary($dir,$type,$tag,$run,$range,$theLevel,$theSummary) . "'>{$theLevel} " .str_replace("_"," ",$theSummary) . " Summary</a></li>\n"; 
      }
      echo "</ul>\n";
    }
  }
}


function content_displayConfiguration() { 
  global $dir, $type, $tag, $run, $config, $search;  

  echoHeader(3, "Trigger Configuration Summary:");
  $configTable = new ConfigurationTable( getConfig($dir, $type, $tag, $run, $config) );
  $configTable->setSearch($search);
  $configTable->insertConfigTable();
}



function content_displaySummary() {
  global $dir, $type, $tag, $run, $range, $level, $summary;
  
  //echoHeader(3, "{$level} {$summary} Summary:");
  $csv = getCsv($dir, $type, $tag, $run, $range, $level, $summary);
  $costTable = new CostDataTable();
  $costTable->setTitle("$summary Summary");
  $costTable->setDataSource( $csv, $dir, $type, $tag, $run, $range, $level, $summary );

  if ($summary == "Full_Evnt" || $summary == "Slow_Evnt") {
    $costTable->addSearchCol(2); // Chain name
    $costTable->addSearchCol(3); // Alg name
    $costTable->addSearchCol(4); // Seq name
    $costTable->addSearchCol(14); // ROS
    $costTable->setSortCol(1,"asc");
    echo "<p>Each algorithm may be run many times in an event, the number in square brackets at the end of the algorithm's name indicates which execution it was.</p>";
  }

  if (strpos($summary, "Rate_") !== FALSE) {
    $costTable->hideColumn(1); // Time
    $costTable->addSearchCol(2); // Group
    # Print out the L
    $lumiVal = 0;
    $cmtStr = "";
    $lumiStr = "";
    getRatesLumiPoint($dir, $type, $tag, $run, $lumiVal, $lumiStr, $cmtStr);
    $predStr = "prediction for " . $lumiStr;
    if (strpos($dir, "Online") !== FALSE) $predStr = "for";
    if ($lumiVal > 0) echo "<p><b>Rates " . $predStr ." L = " . sprintf("%.2e",$lumiVal) . " cm<sup>-2</sup>s<sup>-1</sup></b></p>";
    if ($cmtStr != "") echo "<p><b>Cache: ${cmtStr}</b></p>";
    echo "<form action='?page=CompareRates' method='post'><input type='hidden' name='compA' value='{$_SERVER['REQUEST_URI']}'/>";
    echo "<p>Rate Comparison: <input type='text' name='compB' value='Paste the full URL of another Rates Summary page here' /><input type='submit' value='Do Comparison'/></p></form>";
  }

  if (strpos($summary, "Chain") !== FALSE) {
    $costTable->addSearchCol(1); // Group
  }

  $costTable->insertTable();

  // These ones get a fancy pi chart
  if (strpos($summary, "Group") !== FALSE) $costTable->insertRatesPiChart();
  if (strpos($summary, "SliceCPU") !== FALSE) $costTable->insertCPUPiChart();
}



function content_displayItem() {
  global $dir, $type, $tag, $run, $range, $level, $summary, $summaryDisplay, $item, $err;
 
  if ( checkRoot($dir, $type, $tag, $run) == 0 ) {
    $err[] = "ERROR: Cannot find ROOT file for this run, unable to show histograms.";
    echoErrorMsgs(); 
  }

  echoHeader(3, "{$summaryDisplay} {$item} Details:");
  $csv = getCsv($dir, $type, $tag, $run, $range, $level, $summary);
  $costTable = new CostDataTable();
  $costTable->setDataSource( $csv, $dir, $type, $tag, $run, $range, $level, $summary, $item );
  $success = $costTable->insertItemDetail();
  if ($success == false) {
    $err[] = "ERROR: Unable to find details for this item.";
    echoErrorMsgs(); 
  }

  $configs = getTrigConfigs($dir, $type, $tag, $run);
  
  // Display secondary table
  if ($summary == 'Sequence' || $summary == 'Chain') {
    // Use config table if any, otherwise use metadata
    if (count($configs) > 0) {
      $config = $configs[0];
      $configTable = new ConfigurationTable( getConfig($dir, $type, $tag, $run, $config ) );
    } else {
      $configTable = new ConfigurationTable( getMetadata($dir, $type, $tag, $run), "json" );
    }
    if ($summary == 'Chain') $myAlgs = $configTable->getChainAlgs( $item );
    else $myAlgs = $configTable->getSeqAlgs( $item );
    $costTable->insertAlgLinkTable($myAlgs);
  }
  
  // Display D3 graph
  if (strpos($summary, "Rate_") !== FALSE) {
    if (checkRatesGraph($dir, $type, $tag, $run) != 0) {
      $ratesGraph = json_decode ( file_get_contents(getRatesGraph($dir, $type, $tag, $run)), true );
      $chainArray = $ratesGraph["children"];
      foreach ($chainArray as $key => &$element) {
        if ($element["text"] != $item) continue;
        $ratesGraph = new RatesGraph();
        $ratesGraph->insertRatesGraph( json_encode($element["children"]) );
        break;
      }
    }
  }
 
  echo "<div class='clearBoth'></div>";
  
  if ($summary == 'Algorithm' || $summary == 'Sequence' || $summary == 'Chain' || $summary == 'Rate_ChainHLT' || $summary == 'Rate_ChainL1') {
    echoHeader(3,"Trigger Configurations:"); // Link to trig configs
    
    echo "<ul>\n";
    // Check if this item has an instance name on the end, e.g. [1]
    $pos = strpos($item, "[");
    if ($pos !== FALSE) $itemNoInstance = substr($item, 0, $pos);
    else $itemNoInstance = $item;
    foreach($configs as &$theConfig) {
      echo "  <li><a href='" . getLinkConfigSearch($dir,$type,$tag,$run,$theConfig,$itemNoInstance) . "'> Lookup {$itemNoInstance} in " . str_replace("_"," ",$theConfig) . "</a>";
      $lumiBlocks = getLumiBlocksFromMeta($dir, $type, $tag, $run, $theConfig);
      if ($lumiBlocks !== "") echo " (Lumi Blocks:" . $lumiBlocks . ")";
      echo "</li>\n"; 
    }
    echo "</ul>\n";
  }
  
  echoHeader(3, "{$item} Histograms:");
  $costHisto = new CostHistograms( getRoot($dir, $type, $tag, $run) );
  $costHisto->addDirectory($range);
  $costHisto->addDirectory("{$summary}_{$level}");
  $costHisto->addDirectory($item);
  $costHisto->insertHistograms();
}



function page_about() {
echo <<< EXPORT
<p>Trig Cost Browser is developed by Tim Martin &lt;<a href='mailto:Tim.Martin@cern.ch'>Tim.Martin@cern.ch</a>&gt;</p>
<p>The framework to extract and process trigger monitoring data for display is developed by the <a href='https://twiki.cern.ch/twiki/bin/view/Atlas/AtlasTriggerRates'>Trigger Rates Group</a>: Elliot Lipeles, Rustem Ospanov, Doug Schaefer, Rami Vanguri, Tae Min Hong (Penn); Tim Martin, Elisabetta Pianori (Warwick); Moritz Backes, Yu Higuchi (CERN); &lt;<a href='mailto:atlas-trigger-rate-expert@cern.ch'>ATLAS-Trigger-Rate-Expert@cern.ch</a>&gt;</p>
<h3>Technologies</h3>
<p>This site has been developed using the following tools:</p>
<ul>
<li><a href='http://dabuttonfactory.com/'>Da Button Factory</a></li>
<li><a href='http://www.csstablegenerator.com/'>CSS Table Generator</a></li>
<li><a href='http://d3js.org/'>D3.js JavaScript library</a></li>
<li><a href='http://flexigrid.info/'>Flexigrid</a></li>
<li><a href='https://gist.github.com/andyferra/2554919'>Github Markdown CSS</a></li>
<li><a href='http://jquery.com/'>jQuery</a></li>
<li><a href='http://www.jstree.com/'>jsTree</a></li>
<li><a href='http://www.php.net/'>PHP</a></li>
<li><a href='http://root.cern.ch/js/'>ROOT js</a></li>
</ul>
EXPORT;
}



function page_symlinks() {
  echo "<p>Running Setup...</p>";
}


function page_log() {
  global $page, $key;
  if ($page == 'log') {
    echo "<h3>Local cron log (P1 Data):</h3>";
    $log = file('/data/cron.log');
    if ($key == 'breakSleep') {
      file_put_contents("/var/www/TrigCostWeb/breakSleep", "breakSleep");
      echo "<p>Wake-up sent. It will be picked up within 60s.</p>";
    }
  } else {
    echo "<h3>Local cron log (Manual Requests):</h3>";
    $log = file('/data/cronManual.log');
    if ($key == 'breakSleep') {
      file_put_contents("/var/www/TrigCostWeb/breakSleepManual", "breakSleepManual");
      echo "<p>Wake-up sent. It will be picked up within 60s.</p>";
    }
  }
  echo "<p>Click <a href='" . getLinkPage($page) . "&key=breakSleep'><strong>here</strong></a> to check for new data to process</p>";
  $total = count($log);
  $toGet = min($total, 1000);
  echo "<p class='codeParaSmall'>";
  for ($i = $total-1; $i > $total-$toGet; $i--) {
    echo htmlentities($log[$i]) . "<br />";
  }
  echo "</p>";
}



function page_software() {
  echo "<h3>Installed Software:</h3>";
  echo "<p class='codePara'>";
  echo exec("svn info /opt/trig-cost-sw/TrigCostRootAnalysis/ | grep ^URL") . "<br />";
  echo exec("svn info /opt/trig-cost-sw/TrigCostD3PD/ | grep ^URL") . "<br />";
  echo exec("svn info /opt/trig-cost-sw/TrigRootAnalysis/ | grep ^URL") . "<br />";
  echo "</p>";
}



function page_unknown() {
  echo "<p>Unknown Page</p>";

}

?>
