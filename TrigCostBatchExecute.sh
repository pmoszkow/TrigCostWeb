# This script runs the analysis
echo "Execute] Filelist to process is: $1" # Must be supplied
echo "Execute] Directory to output is: $2" # Must be supplied
echo "Execute] Tag is: $3"
echo "Execute] Details is: $4"

################################################## ##################################################
HOME=/opt/trig-cost-sw
################################################## ##################################################

if [ -z "$1" ]; then # No run dir - fatal
  echo "Execute] No file list supplied, exit."
  exit
fi

if [ -z "$2" ]; then # No our dir - fatal
  echo "Execute] No output directory supplied, exit."
  exit
fi

# Go home
cd $HOME

#setup eos
echo "Execute] Setup EOS"
export EOS_MGM_URL=root://eosatlas.cern.ch

kinit -5 attradm@CERN.CH -k -t $HOME/keytabs/attradm.keytab # Authenticate

# Setup release
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
echo "Execute] Source atlasLocalSetup.sh"
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
echo "Execute] Setup athena"
source ${AtlasSetup}/scripts/asetup.sh AthenaP1,21.1.41

# Prepare run command
#pp
#RUN="RunTrigCostD3PD --cleanAll --messageWait 120 --fileList $1 --outputRootDirectory=$2 \
#--summaryPerHLTConfig --summaryPerLumiBlock \
#--nHLTConfigSummary=2 --nLbFullSummary=10 --nLbPerHLTConfig=3 --nLbFullSkip=150 \
#--monitorROI --monitorGlobals --monitorFullEvent \
#--monitorChains --monitorChainAlg --monitorSequences --monitorAlgs --monitorAlgClass --monitorSliceCPU --monitorROS \
#--nThreads 1 --basicWeight 10"

#--monitorROS --monitorROSAlgs --monitorROSChains \

#hi
RUN="RunTrigCostD3PD --cleanAll --messageWait 120 --fileList $1 --outputRootDirectory=$2 \
--summaryPerHLTConfig --summaryPerLumiBlock \
--nHLTConfigSummary=3 --nLbFullSummary=10 --nLbPerHLTConfig=5 --nLbFullSkip=100 \
--monitorROI --monitorGlobals --monitorFullEvent \
--monitorChains --monitorChainAlg --monitorSequences --monitorAlgs --monitorAlgClass --monitorSliceCPU \
--nThreads 1 --basicWeight 10"

if [ ! -z "$3" ]; then # If a tag provided
  RUN="$RUN --outputTag $3"
fi

if [ ! -z "$4" ]; then # If user details provided
  RUN="$RUN --userDetails $4"
fi

echo "Execute] COMMAND: $RUN"
$RUN

echo "Double-removing progress file"
rm -v /data/data*/costMonitoring_*/progress.json
